import { GameManager, SCENE } from '../Managers/GameManager';
import { MainManager } from '../Main/MainManager';
declare var firebase : any;

const {ccclass, property} = cc._decorator;

@ccclass
export default class LoginPanel extends cc.Component {

    @property(cc.EditBox)
    email : cc.EditBox = null;
    @property(cc.EditBox)
    password : cc.EditBox = null;
    loginPanel: cc.Node = null;
    loginButton : cc.Node = null;
    optionButton : cc.Node = null;
    creditButton : cc.Node = null;
    loginbackground : cc.Node = null;
    guestButton : cc.Node = null;
    start(){
        this.loginPanel = cc.find('Canvas/Login Panel');
        this.optionButton = cc.find('Canvas/Main Screen/Buttons/Option');
        this.loginButton = cc.find('Canvas/Main Screen/Buttons/Login');
        this.creditButton = cc.find('Canvas/Main Screen/Buttons/Credit');
        this.loginbackground = cc.find('Canvas/Main Screen/Buttons/Login background');
        this.guestButton = cc.find('Canvas/Main Screen/Buttons/Guest');
    }
    return(){
        this.scheduleOnce(function(){
            this.loginButton.active = true;
            this.optionButton.active = true;
            this.creditButton.active = true;
            this.loginbackground.active = true;
            this.guestButton.active = true;
        }, 0.2);
        
        cc.tween(this.loginPanel)
        .to(0.5, {scale: 0.2, opacity: 0}, {easing: "quartInOut"})
        .call(()=>{this.loginPanel.active = false})
        .start();
    }
    emailToPassword(){
        this.password.focus();
    }
    signIn(){
        firebase.auth().signInWithEmailAndPassword(this.email.string, this.password.string).then((result) => {
            // this.create_alert('success', "Account Signed In!");
            console.log(result);
            var uid = firebase.auth().currentUser.uid;
            GameManager.Instance.userUID = uid;
            GameManager.Instance.userEmail = firebase.auth().currentUser && firebase.auth().currentUser.email;
            firebase.database().ref('users/' + uid + '/username').once('value', (snapshot)=>{
                GameManager.Instance.username = snapshot.val();
            });

            firebase.database().ref('users/' + uid).update({
                timestamp: Date.now()
            })
            .then(()=>{
                // TODO : add some transitioning here
                console.log('load main scene');
                cc.director.loadScene(SCENE.MAIN);
            });

        }).catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
            // this.create_alert('error', errorMessage);
        });
    }
    signUp(){
        firebase.auth().createUserWithEmailAndPassword(this.email.string, this.password.string).then((result) => {
            // this.create_alert('success', "Account Signed Up!");
            var uid = firebase.auth().currentUser.uid;
            GameManager.Instance.userUID = uid;
            GameManager.Instance.userEmail = firebase.auth().currentUser && firebase.auth().currentUser.email;

            // TODO : more user info ?
            firebase.database().ref('users/').update({
                [uid] : {
                    username : "",
                    timestamp: Date.now()
                }
            })
            .then(()=>{
                // TODO : add some transitioning here
                cc.director.loadScene(SCENE.MAIN, ()=>{
                    this.scheduleOnce(()=>{
                        MainManager.Instance.openCreateUsernameModal();
                    }, 0.3);
                });
            }).catch((error)=>{
                alert(error);
            });

        }).catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
            // this.create_alert('error', errorMessage);
            alert(error);
        });
    }

}
