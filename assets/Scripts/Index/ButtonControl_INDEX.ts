// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import { GameManager, SCENE } from "../Managers/GameManager";
const {ccclass, property} = cc._decorator;

@ccclass
export default class ButtonCOntrol_INDEX extends cc.Component {
    transition : cc.Node = null;
    @property({type:cc.AudioClip})
    hoverEffect: cc.AudioClip = null;
    @property({type: cc.AudioClip})
    pressEffect: cc.AudioClip = null;
    onLoad(){
        this.node.on('mouseenter', this.upscale, this);
        this.node.on('mouseleave', this.downscale, this)
    }
    onDestroy(){
        this.node.off('mouseenter', this.upscale, this);
        this.node.off('mouseleave', this.downscale, this)
    }
    upscale(){
        cc.audioEngine.playEffect(this.hoverEffect, false);
        document.body.style.cursor = "pointer";
        cc.tween(this.node)
        .to(0.2, {scale: 1.1})
        .start()
    }
    downscale(){
        document.body.style.cursor = "auto";
        cc.tween(this.node)
        .to(0.2, {scale: 1.0})
        .start()
    }
    click(){
        cc.audioEngine.setEffectsVolume(0.5);
        cc.audioEngine.playEffect(this.pressEffect, false);
        this.scheduleOnce(function(){
            cc.audioEngine.setEffectsVolume(1);
        },0.3)
    }
    load(){
        this.scheduleOnce(function(){
            cc.director.loadScene(SCENE.MAIN);
        },1);
        this.transition = cc.find('Canvas/black transition');
        this.transition.active = true;
        this.transition.opacity = 0;
        cc.tween(this.transition)
        .to(1,{opacity: 255})
        .start();
    }
    
    // update (dt) {}
}
