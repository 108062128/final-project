
const {ccclass, property} = cc._decorator;

@ccclass
export default class MainScreen extends cc.Component {

    
    loginPanel : cc.Node = null;
    loginButton : cc.Node = null;
    optionButton : cc.Node = null;
    creditButton : cc.Node = null;
    loginbackground : cc.Node = null;
    guestButton : cc.Node = null;
    start(){
        this.loginPanel = cc.find('Canvas/Login Panel');
        this.optionButton = cc.find('Canvas/Main Screen/Buttons/Option');
        this.loginButton = cc.find('Canvas/Main Screen/Buttons/Login');
        this.creditButton = cc.find('Canvas/Main Screen/Buttons/Credit');
        this.loginbackground = cc.find('Canvas/Main Screen/Buttons/Login background');
        this.guestButton = cc.find('Canvas/Main Screen/Buttons/Guest');
    }
    login(){
        this.scheduleOnce(function(){
            this.optionButton.active = false;
            this.creditButton.active = false;
            this.loginButton.active = false;
            this.loginbackground.active = false;
            this.guestButton.active = false;
        }, 0.2);
        this.loginPanel.active = true;
        this.loginPanel.opacity = 0;
        this.loginPanel.scale = 0.2;
        cc.tween(this.loginPanel)
        .to(0.5,{scale: 1, opacity: 255},{easing:"quartInOut"})
        .start();
    }


}
