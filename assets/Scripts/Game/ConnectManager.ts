import { GameManager, SCENE } from '../Managers/GameManager';
import GameController from './GameController';

declare var firebase : any;
const {ccclass, property} = cc._decorator;

@ccclass
export default class ConnectManager extends cc.Component {

    @property(GameController)
    gameController : GameController = null;

    start (){
        // Local Mode
        if (GameManager.Instance.roomID == ''){
            this.gameController.gameinit(GameManager.Instance.gameInfo);
        }
        // Connect Mode
        else {
            // TODO TODO TODO
        }
    }

    leaveGame(){
        // Do something here
        cc.director.loadScene(SCENE.MAIN);
    }

}