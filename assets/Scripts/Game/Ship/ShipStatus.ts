import Ship from './Ship';
import { SHIPTYPE } from '../../Select/SelectControl';
import GameController from '../GameController';

const {ccclass, property} = cc._decorator;

@ccclass
export default class ShipStatus extends cc.Component {

    // Main control
    @property(cc.Boolean)
    isEnemy : boolean = false;

    // Ship type
    @property(cc.Label)
    class : cc.Label = null;
    
    // Health
    @property(cc.Sprite)
    hexagon : cc.Sprite = null;
    @property(cc.ProgressBar)
    healthBar : cc.ProgressBar = null;
    @property(cc.Label)
    healthText : cc.Label = null;

    // Shield
    @property(cc.ProgressBar)
    cannonDefBar : cc.ProgressBar = null;
    @property(cc.Label)
    cannonDefText : cc.Label = null;
    @property(cc.ProgressBar)
    laserDefBar : cc.ProgressBar = null;
    @property(cc.Label)
    laserDefText : cc.Label = null;
    @property(cc.ProgressBar)
    interceptorDefBar : cc.ProgressBar = null;
    @property(cc.Label)
    interceptorDefText : cc.Label = null;

    ship : Ship = null;
    shipType : SHIPTYPE = SHIPTYPE.NONE;


    cannonDef : number = 0;
    laserDef : number = 0;
    interceptorDef : number = 0;
    preCannonDef : number = 0;
    preLaserDef : number = 0;
    preInterceptorDef : number = 0;
    health : number = 10;
    preHealth : number = 10;
    MAXHEALTH : number = 10;
    lerping : boolean = false;

    inininin(){
        switch(this.shipType){
            case SHIPTYPE.MOTHER:
                this.class.string = 'M';
                this.MAXHEALTH = 15;
                break;
            case SHIPTYPE.ATTACKER:
                this.class.string = 'A';
                this.MAXHEALTH = 12;
                break;
            case SHIPTYPE.SCOUT:
                this.class.string = 'S';
                this.MAXHEALTH = 10;
                break;
        }
    }

    refresh(){
        if (this.lerping) return;
        this.lerping = true;

        var faster : boolean = GameController.Instance.spectate;

        if (this.ship){     // If alive
            this.preCannonDef = this.cannonDef;
            this.preLaserDef = this.laserDef;
            this.preInterceptorDef = this.interceptorDef;
            this.preHealth = this.health;

            this.cannonDef = this.ship.cannonDefense;
            this.laserDef = this.ship.laserDefense;
            this.interceptorDef = this.ship.interceptorDefense;
            this.health = this.ship.health >= 0 ? this.ship.health : 0;    
        }
        else {              // else, died
            // TODO : show a big X on the UI
            this.cannonDef = 0;
            this.laserDef = 0;
            this.interceptorDef = 0;
            this.health = 0;
        }
        const MAXDEF = 3;

        if (!this.isEnemy){
            var lerpRatio = 1;
            this.schedule(()=>{
                this.cannonDefBar.progress      = cc.misc.lerp(this.cannonDef / MAXDEF, this.preCannonDef / MAXDEF, lerpRatio);
                this.laserDefBar.progress       = cc.misc.lerp(this.laserDef / MAXDEF, this.preLaserDef / MAXDEF, lerpRatio);
                this.interceptorDefBar.progress = cc.misc.lerp(this.interceptorDef / MAXDEF, this.preInterceptorDef / MAXDEF, lerpRatio);
                
                this.cannonDefText.string      = Math.round(this.cannonDef).toString();
                this.laserDefText.string       = Math.round(this.laserDef).toString();
                this.interceptorDefText.string = Math.round(this.interceptorDef).toString();

                lerpRatio *= (faster ? 0.85 : 0.92);
            }, 0, (faster ? 12 : 48) );
            this.scheduleOnce(()=>{
                this.cannonDefBar.progress = Math.round(this.cannonDefBar.progress * 100) / 100;
                this.laserDefBar.progress = Math.round(this.laserDefBar.progress * 100) / 100;
                this.interceptorDefBar.progress = Math.round(this.interceptorDefBar.progress * 100) / 100;
            }, 1/60 * (faster ? 12 : 48));
        }

        var healthRatio = 1;
        this.schedule(()=>{
            this.healthBar.progress = cc.misc.lerp(this.health / this.MAXHEALTH, this.preHealth / this.MAXHEALTH, healthRatio);

            this.healthText.string = Math.round(this.health).toString();
            this.hexagon.fillRange = -cc.misc.lerp(this.health / this.MAXHEALTH, this.preHealth / this.MAXHEALTH, healthRatio);

             healthRatio *= (faster ? 0.85 : 0.92);
        }, 0, (faster ? 12 : 48));
        this.scheduleOnce(()=>{
            this.healthBar.progress = Math.round(this.healthBar.progress * 100) / 100;
        }, 1/60 * (faster ? 12 : 48));

        this.scheduleOnce(()=>{
            this.lerping = false;
        }, 1/60 * (faster ? 11 : 47));
        
    }

}
