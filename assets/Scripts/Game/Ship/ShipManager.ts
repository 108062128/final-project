import GameController from '../GameController';
import Ship from './Ship';
import CardZone from '../Card/CardZone';
import { GameManager } from '../../Managers/GameManager';
import ShipStatus from './ShipStatus';
import { SHIPTYPE } from '../../Select/SelectControl';

const {ccclass, property} = cc._decorator;

@ccclass
export default class ShipManager extends cc.Component {

    @property(cc.Prefab)
    myShip : cc.Prefab = null;
    @property(cc.Prefab)
    teamShip : cc.Prefab = null;
    @property(cc.Prefab)
    enemyShip : cc.Prefab = null;
    @property(cc.Prefab)
    playerInfo : cc.Prefab = null;
    @property(cc.Prefab)
    enemyInfo : cc.Prefab = null;

    // Prefab instances' parent
    @property(cc.Node)
    ships : cc.Node = null;
    @property(cc.Node)
    shipStatus : cc.Node = null;

    @property([cc.SpriteFrame])
    shipSpriteFrames : cc.SpriteFrame[] = [];

    playerShip : Ship = null;
    
    _shipsArray : CardZone[] = [];

    init(playerCount : number) : CardZone[]{
        var nums : number[] = [];
        var len = GameController.Instance.shipSpotsArray.length;
        for (var i=0; i<len; i++)
            nums.push(i);

        // Spawn ships on non-repeated ship spots
        var tmpSpotsArray = GameController.Instance.shipSpotsArray;
        for (var i=0; i<playerCount; i++){
            // Get position
            var k = Math.floor(Math.random() * nums.length);
            var targetCardZone = tmpSpotsArray[nums.splice(k, 1)[0]];
            var spawnPos = targetCardZone.node.position;
            var spawnLocation = targetCardZone.location;
            
            // Spawn corresponding ship
            var node : cc.Node = null;
            var status : cc.Node = null;
            var ss : ShipStatus = null;
            // If Player
            if (shipSet[playerCount][i] == 1){
                node = cc.instantiate(this.myShip);
                this.playerShip = node.getComponent(Ship);
                // Using yellow ships (0, 2, 4)
                let rnd = Math.floor(Math.random()*3)*2;
                if (GameManager.Instance && GameManager.Instance.shipChoice != SHIPTYPE.NONE)
                    cc.find("Ship", node).getComponent(cc.Sprite).spriteFrame = this.shipSpriteFrames[GameManager.Instance.shipChoice*2 - 2];
                else
                    cc.find("Ship", node).getComponent(cc.Sprite).spriteFrame = this.shipSpriteFrames[rnd];

                // Spawn ship status UI
                status = cc.instantiate(this.playerInfo);
                this.shipStatus.height = status.height * status.scaleY;         // Initial size of the scroll view
                status.parent = this.shipStatus;
                ss = status.getComponent(ShipStatus);
                ss.shipType = GameManager.Instance.shipChoice;
            }
            // If teammate
            else if (shipSet[playerCount][i] == 2){
                node = cc.instantiate(this.teamShip);
                // Using yellow ships (0, 2, 4)
                let rnd = Math.floor(Math.random()*3)*2;
                cc.find("Ship", node).getComponent(cc.Sprite).spriteFrame = this.shipSpriteFrames[rnd];

                // Spawn ship status UI
                status = cc.instantiate(this.playerInfo);
                this.shipStatus.height += status.height * status.scaleY;
                status.parent = this.shipStatus;
                status.scale = 0.25;
                ss = status.getComponent(ShipStatus);
                ss.shipType = rnd / 2 + 1;
            }
            // If enemy
            else if (shipSet[playerCount][i] == 3){
                node = cc.instantiate(this.enemyShip);
                // Using blue ships (1, 3, 5)
                let rnd = Math.floor(Math.random()*3)*2 + 1;
                cc.find("Ship", node).getComponent(cc.Sprite).spriteFrame = this.shipSpriteFrames[rnd];

                // Spawn ship status UI
                status = cc.instantiate(this.enemyInfo);
                this.shipStatus.height += status.height * status.scaleY;
                status.parent = this.shipStatus;
                ss = status.getComponent(ShipStatus);
                ss.shipType = (rnd-1) / 2;
            }

            // Set parent and position
            node.parent = this.ships;
            node.position = spawnPos;

            // Script init
            // Ship
            var ship = node.getComponent(Ship);
            ship.team = shipSet[playerCount][i];
            ship.type = ss.shipType;
            switch(ss.shipType){
                case SHIPTYPE.MOTHER:
                    ship.health = 15;
                    break;
                case SHIPTYPE.ATTACKER:
                    ship.health = 12;
                    break;
                case SHIPTYPE.SCOUT:
                    ship.health = 10;
                    break;
            }

            // Ship Status
            ss.ship = ship;
            ss.inininin();
            ss.refresh();

            // Cardzone
            var cardZone = node.getComponent(CardZone);
            cardZone.location = spawnLocation;

            // TODO : attach AI for single player, attach 'Agent' for multiplayer
            // If not player
            if (i != 0){    
                var AI = node.addComponent('AI');
                AI.ship = ship;
                AI.cardZone = cardZone;
                GameController.Instance.AIArray.push(AI);
            }

            // Push to array
            this._shipsArray.push(node.getComponent(CardZone));
        }

        return this._shipsArray;
    }
}


// Player count and corresponding team member count
// My      ship = 1
// Team    ship = 2
// Enemy   ship = 3
// Special ship = 4
var shipSet = [
    [],
    [],
    [1, 3],
    [1, 3, 4],
    [1, 2, 3, 3], // ShipSet[4] = 4 people
    [1, 2, 3, 3, 4],
    [1, 2, 2, 3, 3, 3],
    [1, 2, 2, 3, 3, 3, 4],
    [1, 2, 2, 2, 3, 3, 3, 3],
];