import GameController from '../GameController';
import CardZone from '../Card/CardZone';
import { SHIPTYPE } from '../../Select/SelectControl';

const {ccclass, property} = cc._decorator;

@ccclass
export default class Ship extends cc.Component {

    team : number = 0;
    type : SHIPTYPE = SHIPTYPE.NONE;

    health : number = 10;
    cannonDefense : number = 0;
    laserDefense : number = 0;
    interceptorDefense : number = 0;

    damaged(val : number){
        this.health -= val;
    }
    defenseOvertime(){
        this.cannonDefense -= (this.cannonDefense > 0) ? 1 : 0;
        this.laserDefense -= (this.laserDefense > 0) ? 1 : 0;
        this.interceptorDefense -= (this.interceptorDefense > 0) ? 1 : 0;
    }

}
