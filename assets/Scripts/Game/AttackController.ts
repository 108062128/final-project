import GameController from './GameController';
import { CARDTYPE } from './Card/Card';

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class AttackController extends cc.Component {

    @property(cc.Prefab)
    bomb : cc.Prefab = null;
    @property(cc.Prefab)
    laser : cc.Prefab = null;
    @property(cc.Prefab)
    plane : cc.Prefab = null;
    @property(cc.Prefab)
    aim : cc.Prefab = null;
    @property(cc.Node)
    gameField : cc.Node = null;
    @property(cc.Node)
    shipSpots : cc.Node = null;

    @property({
        type : cc.AudioClip
    })bombSFX : cc.AudioClip = null;
    @property({
        type : cc.AudioClip
    })laserSFX : cc.AudioClip = null;
    @property({
        type : cc.AudioClip
    })planeSFX : cc.AudioClip = null;
    selfShip: cc.Node = null;

    start () {
        // this.gameField = cc.director.getScene();
        this.selfShip = GameController.Instance.shipManager.playerShip.node;
    }
    findEnemy(enemyPos : cc.Vec2, type : CARDTYPE){
        if (GameController.Instance.shipManager.playerShip == null ||
            GameController.Instance.shipManager.playerShip.node == null)
            return;

        // Awkward varible declaration
        // Since all node are GameField's children
        var tmp = this.shipSpots.convertToWorldSpaceAR(enemyPos);
        var newEnemyPos = this.gameField.convertToNodeSpaceAR(tmp);

        var tmpp = this.selfShip.parent.convertToWorldSpaceAR(this.selfShip.position);
        var newPos = this.gameField.convertToNodeSpaceAR(tmpp);
        
        var aimIcon = cc.instantiate(this.aim);
        aimIcon.parent = this.gameField;
        aimIcon.setPosition(newEnemyPos);
        
        cc.tween(aimIcon)
            .to(1.5,{angle: 180, scale: 0.6})
        .start();
        
        var pos : cc.Vec2 = cc.v2(
            newEnemyPos.x - newPos.x,
            newEnemyPos.y - newPos.y
        );
        var degree = Math.atan2(pos.x, pos.y) * 180 / Math.PI;

        cc.tween(this.selfShip.getChildByName("Ship"))
            .to(0.5,{angle: -degree})
        .start();

        this.scheduleOnce(function(){
            switch(type){
                case CARDTYPE.CANNON:
                    this.fireBomb(degree, newEnemyPos);
                break;
                case CARDTYPE.LASER:
                    this.fireLaser(degree, newEnemyPos);
                break;
                case CARDTYPE.INTERCEPTOR:
                    this.firePlane(degree, newEnemyPos); 
                break;
            }
        }, 2);

        this.scheduleOnce(function(){
            aimIcon.destroy();
        }, 3.5);
    }
    fireBomb(radians : number, pos : cc.Vec2){
        var attack = cc.instantiate(this.bomb);

        if (this.selfShip == null) return;
        if (attack == null) return;

        attack.parent = this.gameField;
        attack.setPosition(this.selfShip.position);
        attack.angle = -radians;
        attack.active = true;

        cc.audioEngine.playEffect(this.bombSFX, false);

        cc.tween(attack)
            .to(1, {position: pos})
            .call(()=>{
                attack.destroy()
            })
        .start();
    }
    fireLaser(radians : number, pos : cc.Vec2){
        var attack = cc.instantiate(this.laser);
        attack.parent = this.gameField;
        attack.height = pos.mag();

        if (this.selfShip == null) return;
        if (attack == null) return;

        attack.setPosition(this.selfShip.position);
        attack.angle = -radians;
        attack.active = true;

        cc.audioEngine.playEffect(this.laserSFX, false);

        var action = cc.repeatForever(cc.sequence(cc.fadeOut(0.1),cc.fadeIn(0.1)));
        attack.runAction(action);
        
        this.scheduleOnce(function(){
            attack.destroy();
        }, 1);
    }
    firePlane(radians : number, pos : cc.Vec2){
        var attack1 = cc.instantiate(this.plane);
        var attack2 = cc.instantiate(this.plane);

        if (this.selfShip == null) return;
        if (attack1 == null) return;
        if (attack2 == null) return;

        attack1.parent = this.gameField;
        attack2.parent = this.gameField;
        attack1.setPosition(pos.x, pos.y + 500);
        attack2.setPosition(pos.x, pos.y + 800);

        cc.audioEngine.playEffect(this.planeSFX, false);
        cc.tween(attack1)
            .to(0.4, {position: pos})
        .start();
        this.scheduleOnce(function(){
            attack1.destroy();
        }, 0.5);

        this.scheduleOnce(() => {
            cc.audioEngine.playEffect(this.planeSFX, false);
            cc.tween(attack2)
                .to(0.4, {position: pos})
            .start()
        }, 1);
        this.scheduleOnce(function(){
            attack2.destroy();
        }, 1.5);

    }
    // update (dt) {}
}
