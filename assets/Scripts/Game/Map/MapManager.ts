import ShipSpot from './ShipSpot';
import CardZone from '../Card/CardZone';
import GameController from '../GameController';

const {ccclass, property} = cc._decorator;

@ccclass
export default class MapManager extends cc.Component {

    @property(cc.Prefab)
    shipSpot : cc.Prefab = null;
    @property(cc.Node)
    shipSpots : cc.Node = null;
    
    _shipSpotsArray : CardZone[] = [];

    // Spawn ship spot
    init(mapSize : number) : CardZone[]{
        for (var j=0; j<mapSize; j++)
        for (var i=0; i<mapSize; i++){
            var node = cc.instantiate(this.shipSpot);
            node.parent = this.shipSpots;
            node.position = cc.v2(
                (i-Math.floor(mapSize/2)) * 250,
                (j-Math.floor(mapSize/2)) * 250
            );
            node.getComponent(CardZone).location = cc.v2(i, j);
            this._shipSpotsArray.push(node.getComponent(CardZone));
        }
        return this._shipSpotsArray;
    }

    closeFog(){
        if (GameController.Instance.shipManager.playerShip.node == null) return;

        var myShipLocation: cc.Vec2 = GameController.Instance.shipManager.playerShip.node.getComponent(CardZone).location;

        GameController.Instance.shipsArray.forEach((cardZone : CardZone)=>{
            var particleSystem : cc.ParticleSystem = cardZone.node.getChildByName('Fog').getComponent(cc.ParticleSystem);
            var isInVision : boolean = (myShipLocation.sub(cardZone.location).mag() <= GameController.Instance.defaultVisionLen);

            if (particleSystem.duration == 0  && !isInVision){
                particleSystem.duration = -1;
                particleSystem.resetSystem();
            }
            if (isInVision)
                particleSystem.duration = 0;
        });
        GameController.Instance.shipSpotsArray.forEach((cardZone : CardZone)=>{
            var particleSystem : cc.ParticleSystem = cardZone.node.getChildByName('Fog').getComponent(cc.ParticleSystem);
            var isInVision : boolean = (myShipLocation.sub(cardZone.location).mag() <= GameController.Instance.defaultVisionLen);

            if (particleSystem.duration == 0  && !isInVision){
                particleSystem.duration = -1;
                particleSystem.resetSystem();
            }
            if (isInVision)
                particleSystem.duration = 0;
        });
    }
    closeAllFog(){
        // set ship fog
        GameController.Instance.shipsArray.forEach((cardZone : CardZone)=>{
            var particleSystem : cc.ParticleSystem = cardZone.node.getChildByName('Fog').getComponent(cc.ParticleSystem);
            if (particleSystem.duration == -1) return;
            particleSystem.duration = -1;
            particleSystem.resetSystem();
        });
        // set shipspot fog
        GameController.Instance.shipSpotsArray.forEach((cardZone : CardZone)=>{
            var particleSystem : cc.ParticleSystem = cardZone.node.getChildByName('Fog').getComponent(cc.ParticleSystem);
            if (particleSystem.duration == -1) return;
            particleSystem.duration = -1;
            particleSystem.resetSystem();
        });
    }
    openFog(targetSpot?: cc.Vec2, targetVisionLen?: number){
        if (GameController.Instance.shipManager.playerShip.node == null) return;

        var myShipLocation: cc.Vec2 = GameController.Instance.shipManager.playerShip.node.getComponent(CardZone).location;
        
        // set ship fog
        GameController.Instance.shipsArray.forEach((cardZone : CardZone)=>{
            var particleSystem : cc.ParticleSystem = cardZone.node.getChildByName('Fog').getComponent(cc.ParticleSystem);
            var isInVision : boolean = (myShipLocation.sub(cardZone.location).mag() <= GameController.Instance.defaultVisionLen);

            if (isInVision)
                particleSystem.duration = 0;

            if (targetSpot != undefined && targetVisionLen != undefined){
                if (targetSpot.sub(cardZone.location).mag() <= targetVisionLen){
                    particleSystem.duration = 0;
                }
            }
        });
        // set shipspot fog
        GameController.Instance.shipSpotsArray.forEach((cardZone : CardZone)=>{
            var particleSystem : cc.ParticleSystem = cardZone.node.getChildByName('Fog').getComponent(cc.ParticleSystem);
            var isInVision : boolean = (myShipLocation.sub(cardZone.location).mag() <= GameController.Instance.defaultVisionLen);

            if (isInVision)
                particleSystem.duration = 0;
            
            if (targetSpot != undefined && targetVisionLen != undefined){
                if (targetSpot.sub(cardZone.location).mag() <= targetVisionLen){
                    particleSystem.duration = 0;
                }
            }
        });
    }
    openAllFog(){
        // set ship fog
        GameController.Instance.shipsArray.forEach((cardZone : CardZone)=>{
            var particleSystem : cc.ParticleSystem = cardZone.node.getChildByName('Fog').getComponent(cc.ParticleSystem);
            particleSystem.duration = 0;
        });
        // set shipspot fog
        GameController.Instance.shipSpotsArray.forEach((cardZone : CardZone)=>{
            var particleSystem : cc.ParticleSystem = cardZone.node.getChildByName('Fog').getComponent(cc.ParticleSystem);
            particleSystem.duration = 0;
        });
    }

}
