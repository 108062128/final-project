
const {ccclass, property} = cc._decorator;

@ccclass
export default class EnergyDisplayer extends cc.Component {

    @property(cc.Label)
    currentEnergy : cc.Label = null;
    // @property(cc.Label)
    // currentMaxEnergy : cc.Label = null;
    @property(cc.Node)
    mask : cc.Node = null;

    maxMaskHeight : number = 160;

    start () {
        this.maxMaskHeight = this.mask.height;
    }

    updateDisplay(cur : number, curMax : number){
        this.currentEnergy.string = cur.toString();
        // this.currentMaxEnergy.string = curMax.toString();
        this.mask.height = this.maxMaskHeight * (cur / curMax);
    }

}
