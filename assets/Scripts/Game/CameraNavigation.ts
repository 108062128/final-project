
const {ccclass, property} = cc._decorator;

@ccclass
export default class CameraNavigation extends cc.Component {

    @property(cc.Node)
    canvas : cc.Node = null;
    @property(cc.Node)
    gameField : cc.Node = null;

    @property(cc.Integer)
    steps : number = 100;

    camera : cc.Camera = null; // Main camera

    deltaX : number = 100;
    currentViewX : number = 960;
    previousViewX : number = 960;
    minViewX : number = 960; // 960
    maxViewX : number = 4800;

    mouseDown: boolean = false;

    boundRight: number = 1275;
    boundLeft: number = -300;
    boundBottom: number = -110;
    boundTop: number = 715;

    @property(cc.Graphics)
    graphic : cc.Graphics = null;
    screenRatioXtoY : number = 640/960;
    dirty : boolean = true;

    start () {
        this.currentViewX = this.canvas.getComponent(cc.Canvas).designResolution.width;
        this.minViewX = this.canvas.getComponent(cc.Canvas).designResolution.width;
        this.maxViewX = this.gameField.width/1.5;
        this.deltaX = (this.maxViewX - this.currentViewX) / this.steps;
        this.gameField.on(cc.Node.EventType.MOUSE_WHEEL, this._onMouseScroll, this);
        this.gameField.on(cc.Node.EventType.MOUSE_DOWN, this._onMouseDown, this);
        this.gameField.on(cc.Node.EventType.MOUSE_MOVE, this._onMouseMove, this);
        this.gameField.on(cc.Node.EventType.MOUSE_UP, this._onMouseUp, this);
        this.camera = this.getComponent(cc.Camera);
    }

    // TODO 
    // Draw yellow rect indicator outside the canvas,
    // to show the displaying area on minimap
    update (dt : number) {
        this.graphic.node.position = this.node.position;
        if (this.dirty){
            this.dirty = false;
            this.graphic.clear();

            let currentViewY = this.currentViewX * this.screenRatioXtoY;
            let deltaViewX = this.currentViewX - this.minViewX;
            let deltaViewY = deltaViewX * this.screenRatioXtoY;
            let deltaDistanceX = this.node.position.x - this.canvas.width/2;
            let deltaDistanceY = this.node.position.y - this.canvas.height/2;

            let topRight : cc.Vec2 = cc.v2(
                this.node.position.x + deltaViewX/2 - deltaDistanceX,
                this.node.position.y + deltaViewY/2 - deltaDistanceY
            );
                
            let offset = this.graphic.lineWidth / 2;
            this.graphic.rect(
                topRight.x + offset,
                topRight.y + offset,
                -(this.currentViewX + offset * 2),
                -(currentViewY + offset * 2)
            );
            this.graphic.stroke();
        }

    }

    _onMouseScroll(event : cc.Event.EventMouse){
        // scroll forward : positive, zoom in
        
        this.previousViewX = this.currentViewX;
        if (this.currentViewX > this.minViewX && event.getScrollY() > 0)
            this.currentViewX -= this.deltaX;
        else if (this.currentViewX < (this.maxViewX) * 0.95 && event.getScrollY() < 0)
            this.currentViewX += this.deltaX;

        this.camera.zoomRatio = this.minViewX / this.currentViewX; // the greater zoom ratio, the bigger object 
        this.dirty = true;
    }
    _onMouseDown(event : cc.Event.EventMouse){
        // Consider when camera moved to other place, 
        // the zoom in/out effects should also be restricted
        if(event.getButton() == 2){
            this.mouseDown = true
        }
    }
    _onMouseMove(event : cc.Event.EventMouse){
        // TODO 
        // Parameterize this method base on map size
        var deltaX: number = 0;
        var deltaY: number = 0;
        if(this.mouseDown == true){
            if(event.getDeltaX() >= 0){ // mouse move right => camera move left
                if(this.camera.node.position.x > this.boundLeft){
                    deltaX = event.getDeltaX() * 1;
                }
            }
            if(event.getDeltaX() <= 0){ // mouse move left => camera move right
                if(this.camera.node.position.x < this.boundRight){
                    deltaX = event.getDeltaX() * 1;
                }
            }
            if(event.getDeltaY() >= 0){ // mouse move up => camera move down
                if(this.camera.node.position.y > this.boundBottom){
                    deltaY = event.getDeltaY() * 1;
                }
            }
            if(event.getDeltaY() <= 0){ // mouse move down => camera move up
                if(this.camera.node.position.y < this.boundTop){
                    deltaY = event.getDeltaY() * 1;
                }
            }
            this.camera.node.position = this.camera.node.position.subSelf(cc.v2(deltaX, deltaY));
            this.dirty = true;
        }
    }
    _onMouseUp(event : cc.Event.EventMouse){
        if(this.mouseDown == true){
            this.mouseDown = false;
        }
    }

}
