
const {ccclass, property} = cc._decorator;

@ccclass
export default class AutoHideMinimap extends cc.Component {

    // x = 0.78 ~ 0.995
    @property(cc.Camera)
    wholeViewCamera : cc.Camera = null;

    // x = 0 ~ 200
    @property(cc.Node)
    background : cc.Node = null;

    @property(cc.Vec2)
    sensorShiftPos : cc.Vec2 = cc.v2(0, 0);
    sensorPositionOn : cc.Vec2 = null;
    sensorPositionOff: cc.Vec2 = null;

    @property(cc.Boolean)
    hideByDefault : boolean = true;
    isHidden : boolean = false;

    start(){
        this.sensorPositionOn = this.node.position;
        this.sensorPositionOff = this.sensorShiftPos;
        this.node.on(cc.Node.EventType.MOUSE_DOWN, this.mouseDown, this);

        if (this.hideByDefault)
            this.hide();
    }
    onDisable(){
        this.node.off(cc.Node.EventType.MOUSE_DOWN, this.mouseDown, this);
    }

    mouseDown(event : cc.Event.EventMouse){
        if (this.isHidden == false)
            this.hide();
        else 
            this.show();
    }

    hide(){
        this.isHidden = true;
        this.node.position = this.sensorPositionOff;
        cc.tween(this.wholeViewCamera)
            .to(0.25, {rect : new cc.Rect(0.995, 0.325, 0.2, 0.2)}, {easing : "cubicOut"})
        .start();

        cc.tween(this.background)
            .to(0.25, {position : cc.v2(200, 0)}, {easing : "cubicOut"})
        .start();
    }
    show(){
        this.isHidden = false;
        this.node.position = this.sensorPositionOn;
        cc.tween(this.wholeViewCamera)
            .to(0.25, {rect : new cc.Rect(0.78, 0.325, 0.2, 0.2)}, {easing : "cubicOut"})
        .start();

        cc.tween(this.background)
            .to(0.25, {position : cc.v2(0, 0)}, {easing : "cubicOut"})
        .start();
    }
}
