
const {ccclass, property} = cc._decorator;

@ccclass
export default class BackgroundEnabler extends cc.Component {

    start (){
        this.node.opacity = 255;
    }


}
