import GameController from '../GameController';
const {ccclass, property, requireComponent} = cc._decorator;

@ccclass
@requireComponent(cc.CircleCollider)
export default class CardZone extends cc.Component {

    @property(cc.Node)
    display : cc.Node = null;

    location: cc.Vec2 = cc.v2(0, 0);
    // (y)
    //  2
    //  1   
    //  0     
    //    0 1 2 (x)

    start () {
        this.node.on(cc.Node.EventType.MOUSE_MOVE, (event) => {
            if (GameController.Instance.currentCard != null){
                GameController.Instance.currentCard.display(this);
            }
        });
        this.node.on(cc.Node.EventType.MOUSE_LEAVE, (event) => {
            if (GameController.Instance.currentCard != null){
                GameController.Instance.currentCard.displayReturn();
            }
        });
    }
    // update (dt) {}
}
