import { GameManager } from '../../Managers/GameManager';
import { SHIPTYPE } from '../../Select/SelectControl';
import GameController, { GAMEPHASE } from '../GameController';
import ShipStatus from '../Ship/ShipStatus';
import { CardSubInfo, CARDTYPE } from './Card';
import Card from './Card';
import CardIndicator from './CardIndicator';

declare var firebase : any;
const { ccclass, property } = cc._decorator;

@ccclass
export default class CardManager extends cc.Component {

    @property(cc.Prefab)
    cardPrefab: cc.Prefab = null;
    @property(cc.Node)
    cards : cc.Node = null;
    @property(cc.Node)
    cardLeft : cc.Node = null;
    @property({
        type : cc.Integer,
        displayName : "手牌上限",
        min : 1,
        max : 10,
        step : 1,
        slide : true
    })
    maxCardInHand : number = 6;
    @property({
        type : cc.Integer,
        displayName : "初始手牌數",
        min : 1,
        max : 10,
        step : 1,
        slide : true
    })
    initialHandCard: number = 5;

    // Cards image
    @property([cc.SpriteFrame])
    cardSpriteFrame : cc.SpriteFrame[] = [];

    cardLibrary : cc.Prefab[] = [];
    cardDeck : number[] = [];
    numberOfCardsInHand: number = 0;

    init () {
        // Load all card prefabs under the path : assets/resources/... 
        // TODO : remember this function is ASYNCHROUS
        cc.loader.loadResDir('', cc.Prefab, (error, assets : cc.Prefab[])=>{
            this.cardLibrary = assets;
            if (error || !this.cardLibrary){
                console.log("ERROR : " + error);
                console.log("Card Library : " + this.cardLibrary);
                return;
            }
            
            // Retrieve card deck
            var rawCardDeck : number[] = [];
            var targetCardDeck : number[] = [];
            switch(GameManager.Instance.shipChoice){
                case SHIPTYPE.MOTHER:
                    targetCardDeck = motherCardDeck;
                break;
                case SHIPTYPE.ATTACKER:
                    targetCardDeck = attackerCardDeck;
                break;
                case SHIPTYPE.SCOUT:
                    targetCardDeck = scoutCardDeck;
                break;
                default:
                    targetCardDeck = defaultCardDeck;
                break;
            }
            if (firebase.auth().currentUser == null){
                // Use default card deck
                // Copy value instead of copy ref
                for (var i=0; i<targetCardDeck.length; i++) 
                    rawCardDeck.push(targetCardDeck[i]);
            }
            else {
                // Use database's data (users/[UID]/cards/...)
                // Also ASYNCHROUS
                for (var i=0; i<targetCardDeck.length; i++) 
                    rawCardDeck.push(targetCardDeck[i]);
            }

            // Shuffle deck
            for (var i=0; i<rawCardDeck.length; i++){
                if (rawCardDeck[i] > 0){
                    while (rawCardDeck[i]-- > 0){
                        this.cardDeck.push(i);
                    }
                }
            }
            for (var i = this.cardDeck.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = this.cardDeck[i];
                this.cardDeck[i] = this.cardDeck[j];
                this.cardDeck[j] = temp;
            }

            // Just for developing
            // TODO : remove this after real game play done
            for (let i = 0; i < this.cards.childrenCount; i++){
                var card = this.cards.children[i];
                card.getComponent("Card").positionInHand = this.numberOfCardsInHand++;
                this.updateCardPosition(2);
            }

            // Draw card on top of the deck and remove it from the deck
            this.schedule(() => {
                this.drawCard();
            }, 0.3, this.initialHandCard - 1 - this.numberOfCardsInHand, 0.3);
            
        });
    }

    playCard(playCardPos: number) {
        // Move all cards that were more right to the casted card
        for (let i = 0; i < this.numberOfCardsInHand; i++) {
            var card = this.cards.children[i];
            if (card.getComponent("Card").positionInHand > playCardPos) {
                card.getComponent("Card").positionInHand--;
            }
        }
        this.updateCardPosition(1);
        this.numberOfCardsInHand--;
    }

    drawCard(cardID ?: number) : cc.Node {  // If no param -> draw the card in deck. Else, spawn one
        if (this.numberOfCardsInHand >= this.maxCardInHand) return;
        if (this.cardDeck.length <= 0){
            // out of card = no more card
            return ;
        }

        if (cardID === undefined) 
            cardID = this.cardDeck.pop();

        var newCard = cc.instantiate(this.cardLibrary[cardID]);
        newCard.parent = this.cards;
        newCard.position = cc.v2(10000, 10000);
        newCard.getComponent("Card").positionInHand = this.numberOfCardsInHand++;

        // Delay after card init is complete
        this.scheduleOnce(()=>{
            // Ship type buff
            // Slightly delay, so as to let CI init
            var type = GameController.Instance.shipManager.playerShip.type;
            var info = newCard.getComponent("Card").cardIndicator.getComponent(CardIndicator).cardInfo[GAMEPHASE.ENGAGE];
            if (type == SHIPTYPE.MOTHER){
                // buff interceptor
                if (info.function == CARDTYPE.INTERCEPTOR)
                    info.val++;
            }
            else if (type == SHIPTYPE.ATTACKER){
                // buff laser
                if (info.function == CARDTYPE.LASER)
                    info.val++;
            }
            else if (type == SHIPTYPE.SCOUT){
                // buff cannon
                if (info.function == CARDTYPE.CANNON)
                    info.val++;
            }
            this.updateCardPosition(2);
        }, 0);

        return newCard;
    }

    updateCardPosition(type: number, param_1?: any, param_2?: any) { // 0: not play card, 1: play card, 2: draw card

        // Return uncasted card
        if (type == 0) { // param_1 (number): pos in hand, param_2:
            for (let i = 0; i < this.numberOfCardsInHand; i++) {
                var card = this.cards.children[i];
                var pos = card.getComponent("Card").positionInHand;
                if (pos == param_1) {
                    cc.tween(card)
                        .to(0.5,
                            { position: cardPositionInHand[pos] },
                            { easing: 'cubicOut' })
                        .start()
                }
            }
        }
        // Card casted successfully
        else if (type == 1) {
            for (let i = 0; i < this.numberOfCardsInHand; i++) {
                var card = this.cards.children[i];
                var pos = card.getComponent("Card").positionInHand;
                cc.tween(card)
                    .to(0.5,
                        { position: cardPositionInHand[pos] },
                        { easing: 'cubicOut' })
                    .start()
            }
        }
        // Draw one card
        else if (type == 2) {
            for (let i = 0; i < this.numberOfCardsInHand; i++) {
                var card = this.cards.children[i];
                var pos = card.getComponent("Card").positionInHand;
                card.position = cardPositionInHand[pos];
            }
        }
    }
}

var cardPositionInHand = [
    cc.v2(-240, -10),
    cc.v2(-145, -10),
    cc.v2(-50, -10),
    cc.v2(45, -10),
    cc.v2(140, -10),
    cc.v2(235, -10),
]

var defaultCardDeck = [
    0, 8, 8, 8
]
var motherCardDeck = [
    0, 12, 0, 12
]
var attackerCardDeck = [
    0, 2, 16, 6
]
var scoutCardDeck = [
    0, 14, 8, 2
]