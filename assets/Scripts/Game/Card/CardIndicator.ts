import { CardSubInfo } from './Card';


const {ccclass, property} = cc._decorator;

@ccclass
export default class CardIndicator extends cc.Component {

    @property(cc.Sprite)
    image : cc.Sprite = null;

    @property([CardSubInfo])
    cardInfo : CardSubInfo[] = [];

    init(ci : CardSubInfo[]){
        this.cardInfo = ci;
    }
}
