import CardZone from './CardZone';
import GameController, { GAMEPHASE } from '../GameController';
import CardIndicator from './CardIndicator';
import Ship from '../Ship/Ship';
import ShipSpot from '../Map/ShipSpot';

const { ccclass, property } = cc._decorator;

export enum CARDTYPE {
    SEARCH, 
    SCAN, 
    REVEAL,

    CANNON, 
    LASER, 
    INTERCEPTOR,
    
    MOVE
}

@ccclass('CardSubInfo')
export class CardSubInfo {
    @property({type: cc.Enum(CARDTYPE)})
    function: CARDTYPE = CARDTYPE.SEARCH;

    @property({
        type : cc.Integer,
        displayName : "攻擊力或防禦力",
        min : 0,
        max : 10,
        step : 1,
        slide : true
    })
    val: number = 0;

    @property({
        type : cc.Integer,
        displayName : "費用",
        min : -10,
        max : 10,
        step : 1,
        slide : true
    })
    cost: number = 1;

    @property({
        type : cc.Integer,
        displayName : "最小射程",
        min : 0,
        max : 10,
        step : 1,
        slide : true
    })
    minLimit: number = 0;

    @property({
        type : cc.Integer,
        displayName : "最大射程",
        min : 0,
        max : 50,
        step : 1,
        slide : true
    })
    maxLimit: number = 50;

    constructor(...args:any) {
        // f ?: CARDTYPE, v ?: number, min ?: number, max ?: number
        this.function = args[0];
        this.val = args[1];
        this.cost = args[2];
        this.minLimit = args[3]; //最小射程
        this.maxLimit = args[4]; //最大射程
    }
}

@ccclass
export default class Card extends cc.Component {

    @property(cc.Node)
    cardIndicator : cc.Node = null;
    @property([CardSubInfo])
    cardInfo : CardSubInfo[] = [
        new CardSubInfo(CARDTYPE.SEARCH, 1, 1, 0, 50),
        new CardSubInfo(CARDTYPE.CANNON, 1, 1, 0, 2),
        new CardSubInfo(CARDTYPE.CANNON, 2, 1, 1, 1),
        new CardSubInfo(CARDTYPE.MOVE, 1, 1, 1, 1)
    ];
    
    inCardZone: boolean = false;
    mouseDown: boolean = false;
    canvas : cc.Node = null
    cardManager: cc.Node = null;
    positionInHand: number = -1;

    valid : boolean = false;

    back : cc.Node = null;
    typeValue : cc.Label = null;
    typeImage : cc.Sprite = null;
    energyValue : cc.Label = null;
    distanceValue : cc.Label = null;

    start() {
        this.canvas = cc.find('Game Field');

        this.node.on(cc.Node.EventType.MOUSE_DOWN, this._onMouseDown, this);
        this.canvas.on(cc.Node.EventType.MOUSE_MOVE, this._onMouseMove, this);
        this.node.on(cc.Node.EventType.MOUSE_MOVE, this._onMouseMove, this);
        //当鼠标抬起的时候恢复状态
        this.canvas.on(cc.Node.EventType.MOUSE_UP, this._onMouseUp, this);
        this.node.on(cc.Node.EventType.MOUSE_UP, this._onMouseUp, this);
        
        this.back = this.node.getChildByName("Back");
        this.typeValue = this.node.getChildByName("Type Value").getComponent(cc.Label);
        this.typeImage = this.node.getChildByName("Type Image").getComponent(cc.Sprite);
        this.energyValue = this.node.getChildByName("Energy Value").getComponent(cc.Label);
        this.distanceValue = this.node.getChildByName("Distance Value").getComponent(cc.Label);

        this.updateCardWithPhase();

        this.cardIndicator.getComponent(CardIndicator).init(this.cardInfo);
        this.cardIndicator.opacity = 150;
        this.cardIndicator.active = false;

        this.back.color = BackColor[GameController.Instance.currentDisplayPhase];
    }

    updateCardWithPhase(){
        // TODO : some more color/style
        if (GameController.Instance.shipManager.playerShip == null ||
            GameController.Instance.shipManager.playerShip.node == null)
            return ;

        if (this.typeValue == null) return;

        var phase = GameController.Instance.currentDisplayPhase;
        this.typeValue.string      = this.cardInfo[phase].val.toString();
        this.typeImage.spriteFrame = GameController.Instance.cardManager.cardSpriteFrame[this.cardInfo[phase].function];
        this.energyValue.string    = this.cardInfo[phase].cost.toString();

        this.cardIndicator.getChildByName("Image").getComponent(cc.Sprite).spriteFrame = this.typeImage.spriteFrame;
        this.back.color = BackColor[phase];

        this.distanceValue.node.scale = 1;
        if (this.cardInfo[phase].maxLimit >= 50){
            this.distanceValue.string = '∞';
            this.distanceValue.node.scale = 1.3;
        }
        else if (this.cardInfo[phase].maxLimit == this.cardInfo[phase].minLimit){
            this.distanceValue.string = this.cardInfo[phase].minLimit.toString();
        }
        else {
            this.distanceValue.string = this.cardInfo[phase].minLimit.toString() + '-' + this.cardInfo[phase].maxLimit.toString();
        }
    }

    onDestroy (){
        this.node.off(cc.Node.EventType.MOUSE_DOWN, this._onMouseDown, this);
        this.canvas.off(cc.Node.EventType.MOUSE_MOVE, this._onMouseMove, this);
        this.node.off(cc.Node.EventType.MOUSE_MOVE, this._onMouseMove, this);
        //当鼠标抬起的时候恢复状态
        this.canvas.off(cc.Node.EventType.MOUSE_UP, this._onMouseUp, this);
        this.node.off(cc.Node.EventType.MOUSE_UP, this._onMouseUp, this);
    }


    display(target : CardZone){
        if (!this.validate(target)) return;
        
        this.cardIndicator.parent = target.display;
        
        // Change opacity after CI attached to other node
        this.node.opacity = 100;
        
        // Layout
        if(target.display.childrenCount == 1){
            target.display.height = this.cardIndicator.height;
            target.display.width = this.cardIndicator.width;
        }
        else if(target.display.childrenCount == 2){
            target.display.width = this.cardIndicator.width * 2;
        }
        else if(target.display.childrenCount == 3){
            target.display.height = this.cardIndicator.height * 2;
        }

        // TODO cool transition
        this.cardIndicator.active = true;
        this.inCardZone = true;
    }
    displayReturn(){
        // TODO cool transition
        if(this.cardIndicator.parent.name == "Display"){
            if(this.cardIndicator.parent.childrenCount == 2){
                this.cardIndicator.parent.width = this.cardIndicator.width;
            }
            else if(this.cardIndicator.parent.childrenCount == 3){
                this.cardIndicator.parent.height = this.cardIndicator.height;
            }
        }
            
        this.cardIndicator.parent = this.node;
        this.cardIndicator.active = false;
        this.inCardZone = false;
        this.node.opacity = 255;
        this.back.color = BackColor[GameController.Instance.currentDisplayPhase];
    }

    validate(target : CardZone) : boolean {     // Test if this card can cast on the target
        var phase = GameController.Instance.currentPhase;
        var info = this.cardInfo[phase];

        let min = info.minLimit;
        let max = info.maxLimit;
        let myLocation = GameController.Instance.shipManager.playerShip.getComponent(CardZone).location;
        var distance = myLocation.sub(target.location).mag();
        if (
            ( (distance >= min && distance <= max) || max >= 50) &&    // If distance is reachable
            ( GameController.Instance.currentEnergy >= info.cost )     // If energy > cost
        )   this.valid = true;
        else 
            this.valid = false;

        // Propel cards can't cast on Ship
        if (
            ( phase == GAMEPHASE.PROPEL ) && (
                ( target.getComponent(Ship) != null ) ||
                ( target.node.getChildByName('Fog').getComponent(cc.ParticleSystem).duration == -1 )
            )
        )   this.valid = false;

        // Deploy and engage cards can't cast on shipspot
        if ( phase == GAMEPHASE.DEPLOY ){
            if (target.getComponent(Ship) == null || target.getComponent(Ship).team == 3)
                this.valid = false;
        }
        if ( phase == GAMEPHASE.ENGAGE ){
            if (target.getComponent(Ship) == null || target.getComponent(Ship).team != 3)
                this.valid = false;
        }

        // Each target can only be cast by 4 cards at most
        // And the fourth CI doesn't belong to the fourth card
        if(target.display.childrenCount >= 4 && this.inCardZone == false) this.valid = false;

        // TODO better indicator
        this.back.color = (this.valid) ? BackColor[GameController.Instance.currentDisplayPhase] : cc.color(160, 0, 0);

        return this.valid;
    }

    _onMouseDown(event){
        if(event._button == 0){ // only the left mouse button can move the card
            GameController.Instance.currentCard = this;
            this.mouseDown = true;
            if (GameController.Instance.currentDisplayPhase != GameController.Instance.currentPhase){
                GameController.Instance.currentDisplayPhase = GameController.Instance.currentPhase;
                GameController.Instance.displayRotateButton(null, GameController.Instance.currentDisplayPhase);
            }
        }
    }
    _onMouseMove(event){
        if (!this.mouseDown) return;

        // Card position follow mouse position
        let delta = event.getLocation();
        let newPosition = this.node.parent.convertToNodeSpaceAR(delta);
        this.node.position = newPosition;
    }
    _onMouseUp(event){
        if (!this.mouseDown) return;

        // Destroy casted card (when in cardZone and the cardZone is valid target to cast)
        if(this.inCardZone == true && this.valid == true){
            var phase = GameController.Instance.currentPhase;

            GameController.Instance.cardManager.playCard(this.positionInHand);
            GameController.Instance.updateEnergy(this.cardInfo[phase].cost);

            // If new propel card is casted, return previous propel card to player
            let cardID : number = parseInt(this.node.name.split(' ')[2]);
            if (GameController.Instance.lastPropelCardID != -1)
                GameController.Instance.returnPropelCard();
            if (phase == GAMEPHASE.PROPEL)
                GameController.Instance.rememberPropelCard(cardID, this.cardIndicator);
            
            this.node.destroy();
        }
        // Return card to hand
        else{
            GameController.Instance.cardManager.updateCardPosition(0, this.positionInHand);
            // TODO better indicator
            this.back.color = BackColor[GameController.Instance.currentDisplayPhase];
            this.node.opacity = 255;
        }
        GameController.Instance.currentCard = null;
        this.mouseDown = false;
    }

}

var BackColor = [
    cc.color(60, 150, 15),
    cc.color(200, 200, 200),
    cc.color(245, 47, 47),
    cc.color(195, 42, 233),
];
