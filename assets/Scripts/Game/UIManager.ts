
import GameController, { GAMEPHASE } from './GameController';
import AutoHideShipStatus from './AutoHideShipStatus';
const {ccclass, property} = cc._decorator;

@ccclass
export default class UIManager extends cc.Component {

    @property(cc.Node)
    stateIcon : cc.Node = null;
    @property(AutoHideShipStatus)
    cardDeckHidden : AutoHideShipStatus = null;

    deploy : cc.Node = null;
    engage : cc.Node = null;
    propel : cc.Node = null;
    search : cc.Node = null;
    nextStage : cc.Node = null;
    curStage : cc.Node = null;
    endTurnButton: cc.Node = null;
    onLoad () {
        this.search = cc.find('/BG/search', this.node);
        this.deploy = cc.find('/BG/deploy', this.node);
        this.engage = cc.find('/BG/engage', this.node);
        this.propel = cc.find('/BG/propel', this.node);
        this.endTurnButton = cc.find('/BG/End Turn Button', this.node);
    }

    start () {
        this.curStage = this.search;
        this.nextStage = this.deploy;
    }

    next(){
        this.endTurnButton.getComponent(cc.Button).interactable = false;

        cc.tween(this.curStage)
            .to(0.5,{ position: cc.v2(9,90)}, {easing:"quintInOut" })
        .start();

        this.scheduleOnce(function(){
            cc.tween(this.nextStage)
                .to(0.5,{ position: cc.v2(9, 40)}, {easing:"quintInOut" })
            .start();
        }, 0.3);

        this.scheduleOnce(function(){
            GameController.Instance.endTurnButton();
        }, 0.8);

        let phase = GameController.Instance.currentPhase;
        this.scheduleOnce(() => {
            if (phase != GAMEPHASE.ENGAGE)
                this.endTurnButton.getComponent(cc.Button).interactable = true;
        }, 0.8);

        if(this.curStage == this.search){
            this.scheduleOnce(function(){
                this.stateIcon.position = cc.v2(-146, 90);
                this.curStage = this.deploy;
                this.nextStage = this.engage;
                this.search.position = cc.v2(9,-2);
            },0.8);
        }
        else if(this.curStage == this.deploy){
            this.scheduleOnce(function(){
                this.stateIcon.position = cc.v2(-51, 90);
                this.curStage = this.engage;
                this.nextStage = this.propel;
                this.deploy.position = cc.v2(9,-2);
            },0.8);
        }
        else if(this.curStage == this.engage){
            this.scheduleOnce(function(){
                this.stateIcon.position = cc.v2(40, 90);
                this.curStage = this.propel;
                this.nextStage = this.search;
                this.engage.position = cc.v2(9,-2);
            },0.8);
        }
        else if(this.curStage == this.propel){
            this.scheduleOnce(function(){
                this.stateIcon.position = cc.v2(-246, 90);
                this.curStage = this.search;
                this.nextStage = this.deploy;
                this.propel.position = cc.v2(9,-2);
            },0.8);
        }

        if (this.cardDeckHidden.isHidden)
            this.stateIcon.position = this.stateIcon.position.addSelf(this.cardDeckHidden.shift);
    }

    fasterNext(){
        this.endTurnButton.getComponent(cc.Button).interactable = false;
        cc.tween(this.curStage)
            .to(0.2,{ position: cc.v2(9,90)}, {easing:"quintInOut" })
        .start();

        this.scheduleOnce(function(){
            cc.tween(this.nextStage)
                .to(0.2,{ position: cc.v2(9, 40)}, {easing:"quintInOut" })
            .start();
        }, 0.1);

        this.scheduleOnce(function(){
            GameController.Instance.endTurnButton();
        }, 0.3);

        if(this.curStage == this.search){
            this.scheduleOnce(function(){
                this.stateIcon.position = cc.v2(-146, 90);
                this.curStage = this.deploy;
                this.nextStage = this.engage;
                this.search.position = cc.v2(9,-2);
            }, 0.2);
        }
        else if(this.curStage == this.deploy){
            this.scheduleOnce(function(){
                this.stateIcon.position = cc.v2(-51, 90);
                this.curStage = this.engage;
                this.nextStage = this.propel;
                this.deploy.position = cc.v2(9,-2);
            }, 0.2);
        }
        else if(this.curStage == this.engage){
            this.scheduleOnce(function(){
                this.stateIcon.position = cc.v2(40, 90);
                this.curStage = this.propel;
                this.nextStage = this.search;
                this.engage.position = cc.v2(9,-2);
            },0.2);
        }
        else if(this.curStage == this.propel){
            this.scheduleOnce(function(){
                this.stateIcon.position = cc.v2(-246, 90);
                this.curStage = this.search;
                this.nextStage = this.deploy;
                this.propel.position = cc.v2(9,-2);
            },0.2);
        }

        if (this.cardDeckHidden.isHidden)
            this.stateIcon.position = this.stateIcon.position.addSelf(this.cardDeckHidden.shift);
    }
    // update (dt) {}
}
