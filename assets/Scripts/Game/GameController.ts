import MapManager from './Map/MapManager';
import ShipManager from './Ship/ShipManager';
import CardManager from './Card/CardManager';
import Ship from './Ship/Ship';
import Card, { CardSubInfo, CARDTYPE } from './Card/Card';
import CardZone from './Card/CardZone';
import CardIndicator from './Card/CardIndicator';
import CardPhaseDisplayer from './CardPhaseDisplayer';
import EnergyDisplayer from './EnergyDisplayer';
import AI from './AI/AI';
import ShipStatus from './Ship/ShipStatus';
import AutoHideShipStatus from './AutoHideShipStatus';
import UIManager from './UIManager';
import { GameManager } from '../Managers/GameManager';
import AttackController from './AttackController';
import AutoHideMinimap from './AutoHideMinimap';

const {ccclass, property, executionOrder} = cc._decorator;

export enum GAMEPHASE {
    SEARCH = 0, 
    DEPLOY,
    ENGAGE,
    PROPEL
}

export enum TUTORAILSTATE {
    NT = 0,         // Not Tutorial
    START,          // Start of the tutorial
    DRB,            // Display Rotate Button
    ENERGY,         
    CARD,
    HEALTH,
    MAP,
    ET,             // End Turn
    Final
}

@ccclass
@executionOrder(1)
export default class GameController extends cc.Component {

    static Instance : GameController = null;

//#region Properties
    // Managers
    @property(MapManager)
    mapManager : MapManager = null;
    @property(ShipManager)
    shipManager : ShipManager = null;
    @property(CardManager)
    cardManager : CardManager = null;
    @property(UIManager)
    uiManager : UIManager = null;
    @property(AttackController)
    attackController : AttackController = null;

    // Nodes
    @property(cc.Node)
    camera : cc.Node = null;
    @property(cc.Node)
    winModal : cc.Node = null;
    @property(cc.Node)
    loseModal : cc.Node = null;
    @property(cc.Node)
    drawModal : cc.Node = null;
    @property(cc.Node)
    youAreDead : cc.Node = null;
    @property(cc.Node)
    shipStatus : cc.Node = null;
    @property(cc.Node)
    tutorial : cc.Node = null;
    
    // UIs
    @property(cc.Node)
    tmpcardPhaseDisplayer : cc.Node = null;
    cardPhaseDisplayer : CardPhaseDisplayer = null;
    @property(EnergyDisplayer)
    energyDisplayer : EnergyDisplayer = null;

    // Hider
    @property(AutoHideShipStatus)
    cardHider : AutoHideShipStatus = null;
    @property(AutoHideShipStatus)
    shipStatusHider : AutoHideShipStatus = null;
    @property(AutoHideMinimap)
    minimapHider : AutoHideMinimap = null;

    // SFX
    @property({type: cc.AudioClip})
    searchSFX : cc.AudioClip = null;
    playSearchOrNot : boolean = false;
    @property({type: cc.AudioClip})
    deploySFX : cc.AudioClip = null;
    playDeployOrNot : boolean = false;
    @property({type: cc.AudioClip})
    propelSFX : cc.AudioClip = null;
    playPropelOrNot : boolean = false;

    @property({type: cc.AudioClip})
    displayRotateSFX : cc.AudioClip = null;
    @property({type: cc.AudioClip})
    winSFX : cc.AudioClip = null;
    @property({type: cc.AudioClip})
    loseSFX : cc.AudioClip = null;
    isPlayingWinOrLose : boolean = false;
    @property({type: cc.AudioClip})
    explodeSFX : cc.AudioClip = null;

    // Fields
    @property({
        type : cc.Integer,
        displayName : "每回合抽幾張",
        min : 1,
        max : 10,
        step : 1,
        slide : true
    })
    cardDrawEachTurn: number = 6;
//#endregion

//#region Game Info
    gameInfo : {mapSize : number, playerCount : number} = null;

    shipSpotsArray : CardZone[] = [];
    shipsArray : CardZone[] = [];
    AIArray : AI[] = [];

    mapSize: number = 0;
    defaultVisionLen : number = 1;
    attackAnimDelay : number = 3.6;

    gameover : boolean = false;
    spectate : boolean = false;
//#endregion

//#region Phase Info
    currentPhase : GAMEPHASE = GAMEPHASE.SEARCH;
    currentDisplayPhase : GAMEPHASE = GAMEPHASE.SEARCH;
    currentCard : Card = null;  // the current holding card
    currentMaxEnergy : number = 5;  // Energy cap for this turn
    currentPhaseEnergy : number = 5;// Energy after preview and before end turn ( TODO )
    currentEnergy : number = 5;     // Energy that are usable
    lastPropelCardID : number = -1;             // Make sure that only one propel(movement) can be done
    lastPropelCardIndicator : cc.Node = null;
    propelPendingArray : Record<string, {node : cc.Node, val : number}[]> = {};    // To solve propel conflict problem
    AITargetArray : {cardInfo : CardSubInfo[], cardZone : CardZone}[] = [];
    propelSolveDelay : number = 0.3;    // Prevent all ships from being seen before fogs cover them
//#endregion

//#region Tutorial Info
    isTutorial: boolean = false;
    introduction: number = 0;
    currentTutorialState: TUTORAILSTATE = TUTORAILSTATE.NT;
    currentTutorialNode: cc.Node = null;
//#endregion

//#region Cocos loop
    onLoad () {
        cc.director.getCollisionManager().enabled = true;

        if (GameController.Instance != null){
            this.node.destroy();
            console.log("Duplicated GameController found !!!");
        }
        else {
            GameController.Instance = this;
        }
    }
    onDestroy () {
        GameController.Instance = null;
    }
    start () {
        this.cardPhaseDisplayer = this.tmpcardPhaseDisplayer.getComponent(CardPhaseDisplayer);
        this.isTutorial = GameManager.Instance.isTutorial;
    }
//#endregion

    // Get Game Info from Connect Manager, then start the game by spawning Nodes
    gameinit (gameInfo) {
        // 0. Get map size, for updating fog
        //    and get gameinfo
        this.gameInfo = gameInfo;
        this.mapSize = gameInfo.mapSize;

        // 1. Define ShipSpots on the map (Map Mng)
        this.shipSpotsArray = this.mapManager.init(gameInfo.mapSize);
        
        // 2. Spawn Ships on the Spots
        this.shipsArray = this.shipManager.init(gameInfo.playerCount);
        
        // 2-0. Get AIs or Agents (index start from 1, since player is 0)
        // TODO : Get AI for single player, but what about multiplayer
        for (var i=1; i<this.gameInfo.playerCount; i++){
            this.AIArray.push( this.shipsArray[i].getComponent(AI) );
        }

        // 2-1. Move main camera to player pos
        this.camera.position = this.camera.convertToWorldSpaceAR(this.shipManager.playerShip.node.position);
        
        // 3. Make fog correctly
        this.mapManager.closeFog();

        // 4. Deal the cards to this player
        this.cardManager.init();

        // 5. Update UI
        this.energyDisplayer.updateDisplay(this.currentEnergy, this.currentMaxEnergy);
        
        this.isTutorial = GameManager.Instance.isTutorial;

        // 6. Tutorial Mode
        if (this.isTutorial){
            this.currentTutorialState = TUTORAILSTATE.START;
            this.tutorial.active = true;
            this.introduction = 0;
            this.currentTutorialNode = this.tutorial.getChildByName("Let's Start");
            this.currentTutorialNode.active = true;
        }
    }

//#region Main Loops
    // TODO (online part)
    // End the turn in single player mode
    endTurnButton(){

        // Main execution
        //   Push result to AI array
        this.executeOtherPlayers();
        //   Execute AI movement
        this.executeAITargetArray();
        //   Execute player movement
        this.executeCards();
        //   Solve propel conflict
        this.propelSolve();
        //   Check if game ends
        this.checkShipsStatus();

        // //   Check if game ends
        // this.checkShipsStatus();

        //   Set display phase and hand card to incoming phase
        this.currentDisplayPhase = this.currentPhase;
        this.scheduleOnce(function(){
            this.displayRotateButton(event, this.currentPhase);
        }, 0);
        
        // Phases' different behavior executed here
        switch (this.currentPhase){
            case GAMEPHASE.SEARCH:
                this.currentPhase++;
                break;
            case GAMEPHASE.DEPLOY:
                this.currentPhase++;
                this.shipStatusHider.show();
                break;
            case GAMEPHASE.ENGAGE:
                this.currentPhase++;
                this.shipStatusHider.show();
                break;
            case GAMEPHASE.PROPEL:          // On turn end
                this.currentPhase = GAMEPHASE.SEARCH;
                
                if (!this.spectate){
                    this.mapManager.closeAllFog();
                    this.scheduleOnce(()=>{
                        this.mapManager.closeFog();
                    }, this.propelSolveDelay * 2);
                }
                    
                // Replenish energy
                if (this.currentMaxEnergy < 10)
                    this.currentMaxEnergy += 1;
                this.currentEnergy = this.currentMaxEnergy;
                this.updateEnergy(0);

                // Defense of all ships will decline
                this.shipsDefenseOvertime();
                // Refresh UI
                this.checkShipsStatus();

                // Draw cards each turn
                this.schedule(() => {
                    this.cardManager.drawCard();
                }, 0.3, Math.min(this.cardDrawEachTurn, this.cardManager.maxCardInHand-this.cardManager.numberOfCardsInHand)
                , 0.3);

                break;
            default:
                console.log("Game Loop 滑出去了");
                break;
        }

        // Play SFX once
        if (this.playSearchOrNot){
            this.playSearchOrNot = false;
            cc.audioEngine.playEffect(this.searchSFX, false);
        }
        if (this.playDeployOrNot){
            this.playDeployOrNot = false;
            cc.audioEngine.playEffect(this.deploySFX, false);
        }
        if (this.playPropelOrNot){
            this.playPropelOrNot = false;
            cc.audioEngine.playEffect(this.propelSFX, false);
        }

        // Reset varibles
        this.lastPropelCardID = -1;
        this.lastPropelCardIndicator = null;
        this.propelPendingArray = {};
        this.AITargetArray = [];
    }
    displayRotateButton(event, CustomEventData){
        if(CustomEventData == 0){
            this.currentDisplayPhase = GAMEPHASE.SEARCH;
        }
        else if(CustomEventData == 1){
            this.currentDisplayPhase = GAMEPHASE.DEPLOY;
        }
        else if(CustomEventData == 2){
            this.currentDisplayPhase = GAMEPHASE.ENGAGE;
        }
        else if(CustomEventData == 3){
            this.currentDisplayPhase = GAMEPHASE.PROPEL
        }else{
            console.log("PhaseDisplay Loop 滑出去了");
        }

        cc.audioEngine.playEffect(this.displayRotateSFX, false);

        // Update cards info
        this.cardManager.cards.children.forEach((card: cc.Node)=>{
            card.getComponent(Card).updateCardWithPhase();
        })

        // Update UI
        this.cardPhaseDisplayer.displayRotate();
    }
//#endregion

//#region Game Execution
    // Deal with the card indicators that are on the field
    // then execute them
    executeCards(){
        if (this.shipManager.playerShip.node == null) return;

        var cnt = 0;
        var phase = this.currentPhase;
        this.shipsArray.forEach((cardZone : CardZone)=>{
            if (cardZone.display.childrenCount != 0){
                if (phase == GAMEPHASE.ENGAGE)
                    this.uiManager.endTurnButton.getComponent(cc.Button).interactable = false;

                cardZone.display.children.forEach((ci : cc.Node)=>{
                    var info = ci.getComponent(CardIndicator).cardInfo;
                    
                    // Only consider delay in ENGAGE phase
                    if (phase != GAMEPHASE.ENGAGE){
                        this.processCardInfo(info, cardZone, true, phase);
                    }
                    else {
                        this.scheduleOnce(()=>{
                            this.processCardInfo(info, cardZone, true, phase);
                            // Refresh UI
                            this.checkShipsStatus();
                        }, this.attackAnimDelay * cnt++);
                    }
                    ci.destroy();
                });

                this.scheduleOnce(()=>{
                    if (phase == GAMEPHASE.ENGAGE)
                        this.uiManager.endTurnButton.getComponent(cc.Button).interactable = true;
                }, this.attackAnimDelay * cnt);
            }
        });

        if (cnt == 0)
            this.uiManager.endTurnButton.getComponent(cc.Button).interactable = true;

        this.shipSpotsArray.forEach((cardZone : CardZone)=>{
            if (cardZone.display.childrenCount != 0){
                cardZone.display.children.forEach((ci : cc.Node)=>{
                    var info = ci.getComponent(CardIndicator).cardInfo;
                    this.processCardInfo(info, cardZone, true, phase);
                    ci.destroy();
                });
            }
        });
    }
    processCardInfo(info : CardSubInfo[], target : CardZone, isPlayer ?: boolean, phase ?: GAMEPHASE){
        if (isPlayer === undefined)
            isPlayer = false;
        if (phase === undefined)
            phase = this.currentPhase;
        if (target == null || target.node == null)
            return;

        var cardInfo = info[phase];

        // Determine if we need to play phase SFX
        if (isPlayer){
            switch(phase){
                case GAMEPHASE.SEARCH:
                    this.playSearchOrNot = true;
                break;
                case GAMEPHASE.DEPLOY:
                    this.playDeployOrNot = true;
                break;
                case GAMEPHASE.PROPEL:
                    this.playPropelOrNot = true;
                break;
            }
        }

        switch (cardInfo.function){
            // Search for random ship (no guarantee that the target is being covered or not)
            case CARDTYPE.SEARCH:
                var rnd = Math.floor(Math.random()*(this.gameInfo.playerCount-1)) + 1;
                this.mapManager.openFog(GameController.Instance.shipsArray[rnd].location, 0);
                break;

            // Increase vision len by [val]
            case CARDTYPE.SCAN:
                this.mapManager.openFog(target.location, this.defaultVisionLen + cardInfo.val);
                break;

            // Reveal targeted location of a radius [val]
            case CARDTYPE.REVEAL:
                this.mapManager.openFog(target.location, cardInfo.val);
                break;

            case CARDTYPE.CANNON:
                var ship = target.getComponent(Ship);
                if (phase == GAMEPHASE.DEPLOY){
                    // If current defense is weaker than incoming card effect
                    // then apply the better one 
                    if (ship.cannonDefense < cardInfo.val)
                        ship.cannonDefense = cardInfo.val;
                }
                else {
                    // If the target doesn't have the defense to defend
                    if (ship.cannonDefense <= 0)
                        ship.damaged(cardInfo.val);

                    if (isPlayer)
                        this.attackController.findEnemy(ship.node.position, CARDTYPE.CANNON);
                }
                break;

            case CARDTYPE.LASER:
                var ship = target.getComponent(Ship);
                if (phase == GAMEPHASE.DEPLOY){
                    if (ship.laserDefense < cardInfo.val)
                        ship.laserDefense = cardInfo.val;
                }
                else {
                    if (ship.laserDefense <= 0)
                        ship.damaged(cardInfo.val);

                    if (isPlayer)
                        this.attackController.findEnemy(ship.node.position, CARDTYPE.LASER);
                }
                break;

            case CARDTYPE.INTERCEPTOR:
                var ship = target.getComponent(Ship);
                if (phase == GAMEPHASE.DEPLOY){
                    if (ship.interceptorDefense < cardInfo.val)
                        ship.interceptorDefense = cardInfo.val;
                }
                else {
                    if (ship.interceptorDefense <= 0)
                        ship.damaged(cardInfo.val);

                    if (isPlayer)
                        this.attackController.findEnemy(ship.node.position, CARDTYPE.INTERCEPTOR);
                }
                break;

            case CARDTYPE.MOVE:
                // location(10, 15) -> "10 15"
                var key = target.location.x.toString() + ' ' + target.location.y.toString();
                if (!this.propelPendingArray[key])
                    this.propelPendingArray[key] = [];
                this.propelPendingArray[key].push({
                    node : this.shipManager.playerShip.node, 
                    val :cardInfo.val
                });
                break;

            default :
                console.log("Something fucked up during Card Execution");
                break;
        }
    }
    rememberPropelCard(cardID : number, ci : cc.Node){
        this.lastPropelCardID = cardID;
        this.lastPropelCardIndicator = ci;
    }
    returnPropelCard(){
        if (this.lastPropelCardID < 0) return;
        this.lastPropelCardIndicator.destroy(); 

        // Have to wait until previous card's node is destroyed
        // so as to let children count in cards behave correctly
        // And for the delay amount, use 0.5 for card in hand to move properly
        var lastID = this.lastPropelCardID;
        this.scheduleOnce(()=>{
            let node = this.cardManager.drawCard(lastID);
            // Return cost of the previous card
            this.updateEnergy(-1 * node.getComponent(Card).cardInfo[GAMEPHASE.PROPEL].cost);
        }, 0.5);
    }
    propelSolve(){              // Solve two-or-more-ship-want-to-move-to-the-same-spot-conflict
        for (let key in this.propelPendingArray){
            if (this.propelPendingArray[key].length > 1){
                // Test if there exists a lowest val of movement?
                // or just random them
                var min = 10000;
                var minNodes : cc.Node[] = [];
                for (var i=0; i<this.propelPendingArray[key].length; i++){
                    if (this.propelPendingArray[key][i].val == min){
                        minNodes.push(this.propelPendingArray[key][i].node);
                    }
                    if (this.propelPendingArray[key][i].val < min){
                        min = this.propelPendingArray[key][i].val;
                        minNodes = [];
                        minNodes.push(this.propelPendingArray[key][i].node);
                    }
                }
                let v2 = this.decodeKey(key);
                let index = v2.x + v2.y * this.gameInfo.mapSize;
                let nodeIndex = Math.floor(Math.random() * minNodes.length);
                minNodes[nodeIndex].getComponent(CardZone).location = v2;
                var tmp1 = minNodes[nodeIndex];
                var tmp2 = this.shipSpotsArray[index].node;
                // this.scheduleOnce(()=>{
                    if (tmp1 == null) return;
                    tmp1.position = tmp2.position;
                // }, this.propelSolveDelay);
            }
            else {
                // move the node to its desired location
                let v2 = this.decodeKey(key);
                let index = v2.x + v2.y * this.gameInfo.mapSize;
                this.propelPendingArray[key][0].node.getComponent(CardZone).location = v2;
                var tmp1 = this.propelPendingArray[key][0].node;
                var tmp2 = this.shipSpotsArray[index].node;
                // this.scheduleOnce(()=>{
                    if (tmp1 == null) return;
                    tmp1.position = tmp2.position;
                // }, this.propelSolveDelay);
            }
        }
    }
    checkShipsStatus(){         // Check if any team wins
        // Team alive count
        // Player = 1, Team = 2, Enemy = 3, Special = 4
        var alive : number[] = [-1, 0, 0, 0, 0];

        // Check for health and remove destroyed ships
        this.shipsArray.slice().reverse().forEach((shipCardZone, index, array)=>{
            var ship = shipCardZone.getComponent(Ship);
            if (ship.health <= 0){
                this.scheduleOnce(()=>{
                    ship.node.getChildByName('Ship').scale = 8;
                    ship.node.getComponent(cc.Animation).play();
                    cc.audioEngine.playEffect(this.explodeSFX, false);
                    this.scheduleOnce(()=>{
                        ship.node.destroy();
                    }, 0.6);
                }, 0.9);
                this.shipsArray.splice(array.length-1-index, 1);
            }
            else alive[ship.team] += 1;
        }, []);

        // Refresh UI
        this.shipStatus.children.forEach((shipInfo : cc.Node)=>{
            var status = shipInfo.getComponent(ShipStatus);
            status.refresh();
        });

        // Check if GAME OVER
        // TODO : transition
        if (alive[1] == 0 && !this.spectate){
            this.spectate = true;
            // Spectator mode
            this.cardHider.hideForever();
            this.mapManager.openAllFog();
            this.uiManager.endTurnButton.getComponent(cc.Button).interactable = false;
            this.youAreDead.active = true;

            this.schedule(()=>{
                if (!this.gameover)
                    this.uiManager.fasterNext();
            }, 0.5, cc.macro.REPEAT_FOREVER, 3);
        }

        if (alive[1] == 0 && alive[2] == 0 && alive[3] == 0){
            // Draw
            this.gameover = true;
            this.youAreDead.active = false;
            this.scheduleOnce(()=>{
                if (!this.isPlayingWinOrLose){
                    this.minimapHider.hide();
                    this.isPlayingWinOrLose = true;
                    cc.audioEngine.playEffect(this.loseSFX, false);
                    this.drawModal.active = true;
                }
            }, 1.0);
        }
        if ((alive[1] != 0 || alive[2] != 0 ) && alive[3] == 0){
            // Team win
            this.gameover = true;
            this.youAreDead.active = false;
            this.scheduleOnce(()=>{
                if (!this.isPlayingWinOrLose){
                    this.minimapHider.hide();
                    this.isPlayingWinOrLose = true;
                    cc.audioEngine.playEffect(this.winSFX, false);
                    this.winModal.active = true;
                }
            }, 1.0);
        }
        if (alive[1] == 0 && alive[2] == 0 && alive[3] != 0){
            // Enemy win
            this.gameover = true;
            this.youAreDead.active = false;
            this.scheduleOnce(()=>{
                if (!this.isPlayingWinOrLose){
                    this.minimapHider.hide();
                    this.isPlayingWinOrLose = true;
                    cc.audioEngine.playEffect(this.loseSFX, false);
                    this.loseModal.active = true;
                }
            }, 1.0);
        }

    }
    shipsDefenseOvertime(){     // Decrease all ship's defensing round
        this.shipsArray.forEach((shipCardZone)=>{
            shipCardZone.getComponent(Ship).defenseOvertime();
        });
    }
//#endregion

//#region MultiPlayer & AI
    executeOtherPlayers(){
        this.AIArray.forEach((AI) => {
            AI.doSomething();
        });
    }
    executeAITargetArray(){
        // AI target array is a virtual array that basically do the same thing as "Card Indicator & Display" combo
        // But it won't take up space, and is not visible by anyone
        this.AITargetArray.forEach((instruction)=>{
            this.processCardInfo(instruction.cardInfo, instruction.cardZone);
        });
    }
//#endregion

//#region UI methods
    // Pass cost = 0 to do pure update
    updateEnergy(cost : number){
        this.currentEnergy -= cost;
        this.energyDisplayer.updateDisplay(this.currentEnergy, this.currentMaxEnergy);
    }
//#endregion

//#region Other Utilities
    decodeKey(str : string) : cc.Vec2 {
        return cc.v2(parseInt(str.split(" ")[0]), parseInt(str.split(" ")[1]));
    }
//#endregion

//#region Tutorial
    nextButton(){
        this.introduction++;
        console.log("in");
        if(this.currentTutorialState == TUTORAILSTATE.START){
            if(this.introduction == 1){
                this.currentTutorialNode.active = false;

                this.currentTutorialState = TUTORAILSTATE.DRB;
                this.currentTutorialNode = this.tutorial.getChildByName("Display Rotate Button Tutorial");
                this.currentTutorialNode.active = true;
                this.introduction = 0;
                
                this.currentTutorialNode.getChildByName("Introduction One").active = true;
            }
        }
        else if(this.currentTutorialState == TUTORAILSTATE.DRB){
            if(this.introduction == 1){
                this.currentTutorialNode.getChildByName("Introduction One").active = false;
                this.currentTutorialNode.getChildByName("Introduction Two").active = true;
            }
            else if(this.introduction == 2){
                this.currentTutorialNode.getChildByName("Introduction Two").active = false;
                this.currentTutorialNode.getChildByName("Introduction Three").active = true;
            }
            else if(this.introduction == 3){
                this.currentTutorialNode.getChildByName("Introduction Three").active = false;
                this.currentTutorialNode.getChildByName("Introduction Four").active = true;
            }
            else if(this.introduction == 4){
                this.currentTutorialNode.getChildByName("Introduction Four").active = false;
                this.currentTutorialNode.getChildByName("Introduction Five").active = true;
            }
            else if(this.introduction == 5){
                this.currentTutorialNode.getChildByName("Introduction Five").active = false;
                this.currentTutorialNode.getChildByName("Introduction Six").active = true;
            }
            else if(this.introduction == 6){
                this.currentTutorialNode.getChildByName("Introduction Six").active = false;
                this.currentTutorialNode.active = false;

                this.currentTutorialState = TUTORAILSTATE.ENERGY;
                this.currentTutorialNode = this.tutorial.getChildByName("Energy Tutorial");
                this.currentTutorialNode.active = true;
                this.introduction = 0;

                this.currentTutorialNode.getChildByName("Introduction One").active = true;
            }
        }
        else if(this.currentTutorialState == TUTORAILSTATE.ENERGY){
            if(this.introduction == 1){
                this.currentTutorialNode.getChildByName("Introduction One").active = false;
                this.currentTutorialNode.active = false;

                this.currentTutorialState = TUTORAILSTATE.CARD;
                this.currentTutorialNode = this.tutorial.getChildByName("Card Tutorial");
                this.currentTutorialNode.active = true;
                this.introduction = 0;

                this.currentTutorialNode.getChildByName("Introduction One").active = true;
            }
        }
        else if(this.currentTutorialState == TUTORAILSTATE.CARD){
            if(this.introduction == 1){
                this.currentTutorialNode.getChildByName("Introduction One").active = false;
                this.currentTutorialNode.getChildByName("Introduction Two").active = true;
            }
            else if(this.introduction == 2){
                this.currentTutorialNode.getChildByName("Introduction Two").active = false;
                this.currentTutorialNode.getChildByName("Introduction Three").active = true;
            }
            else if(this.introduction == 3){
                this.currentTutorialNode.getChildByName("Introduction Three").active = false;
                this.currentTutorialNode.getChildByName("Introduction Four").active = true;
            }
            else if(this.introduction == 4){
                this.currentTutorialNode.getChildByName("Introduction Four").active = false;
                this.currentTutorialNode.active = false;

                this.currentTutorialState = TUTORAILSTATE.HEALTH;
                this.currentTutorialNode = this.tutorial.getChildByName("Health Tutorial");
                this.currentTutorialNode.active = true;
                this.introduction = 0;

                this.currentTutorialNode.getChildByName("Introduction One").active = true;
            }
        }
        else if(this.currentTutorialState == TUTORAILSTATE.HEALTH){
            if(this.introduction == 1){
                this.currentTutorialNode.getChildByName("Introduction One").active = false;
                this.currentTutorialNode.getChildByName("Introduction Two").active = true;
            }
            else if(this.introduction == 2){
                this.currentTutorialNode.getChildByName("Introduction Two").active = false;
                this.currentTutorialNode.active = false;

                this.currentTutorialState = TUTORAILSTATE.MAP;
                this.currentTutorialNode = this.tutorial.getChildByName("Map Tutorial");
                this.currentTutorialNode.active = true;
                this.introduction = 0;

                this.currentTutorialNode.getChildByName("Introduction One").active = true;
            }
        }
        else if(this.currentTutorialState == TUTORAILSTATE.MAP){
            if(this.introduction == 1){
                this.currentTutorialNode.getChildByName("Introduction One").active = false;
                this.currentTutorialNode.getChildByName("Introduction Two").active = true;
            }
            else if(this.introduction == 2){
                this.currentTutorialNode.getChildByName("Introduction Two").active = false;
                this.currentTutorialNode.active = false;

                this.currentTutorialState = TUTORAILSTATE.ET;
                this.currentTutorialNode = this.tutorial.getChildByName("End Turn Tutorial");
                this.currentTutorialNode.active = true;
                this.introduction = 0;

                this.currentTutorialNode.getChildByName("Introduction One").active = true;
            }
        }
        else if(this.currentTutorialState == TUTORAILSTATE.ET){
            if(this.introduction == 1){
                this.currentTutorialNode.getChildByName("Introduction One").active = false;
                this.currentTutorialNode.getChildByName("Introduction Two").active = true;
            }
            else if(this.introduction == 2){
                this.currentTutorialNode.getChildByName("Introduction Two").active = false;
                this.currentTutorialNode.active = false;

                this.currentTutorialState = TUTORAILSTATE.Final;
                this.currentTutorialNode = this.tutorial.getChildByName("Final Tutorial");
                this.currentTutorialNode.active = true;
                this.introduction = 0;

                this.currentTutorialNode.getChildByName("Introduction One").active = true;
            }
        }
        else if(this.currentTutorialState == TUTORAILSTATE.Final){
            if(this.introduction == 1){
                this.currentTutorialNode.getChildByName("Introduction One").active = false;
                this.currentTutorialNode.active = false;
                this.currentTutorialState = TUTORAILSTATE.NT;
                this.isTutorial = false;
                this.tutorial.active = false;
            }
        }
    }
//#endregion

}
