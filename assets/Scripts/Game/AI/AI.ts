import CardZone from '../Card/CardZone';
import Ship from '../Ship/Ship';
import GameController from '../GameController';
import { GAMEPHASE } from '../GameController';
import Card from '../Card/Card';
import { CardSubInfo, CARDTYPE } from '../Card/Card';
import { SHIPTYPE } from '../../Select/SelectControl';

const {ccclass, property} = cc._decorator;

@ccclass
export default class AI extends cc.Component {

    ship : Ship = null;
    cardZone : CardZone = null;
    cardLibrary : cc.Prefab[] = [];

    // TODO : 
    // Advanced AI that can have its own deck, and play the game just as player
    // For now, just let it do random actions to random targets        
    availableSpots : CardZone[] = [];
    overlapSpots : CardZone[] = [];
    simulatedVision : number = 1;
    playerCount : number = 2;
    mapSize : number = 6;
    phase : GAMEPHASE = GAMEPHASE.SEARCH;

    start(){
        this.playerCount = GameController.Instance.gameInfo.playerCount;
        this.mapSize = GameController.Instance.gameInfo.mapSize;
    }

    doSomething(){
        if (this.node == null) return;

        // Lazy init
        if (this.cardLibrary.length == 0)
            this.cardLibrary = GameController.Instance.cardManager.cardLibrary;

        var rnd = Math.random();
        this.phase = GameController.Instance.currentPhase;
        switch(this.phase){
            case GAMEPHASE.SEARCH:
                // Search    
                if (rnd < 1/3){         
                    let rnd2 = Math.floor(Math.random()*this.playerCount);
                    var target = GameController.Instance.shipsArray[rnd2];
                    if (!target) return ;
                    this.getInVisionSpots(0, target.location);
                }
                // Scan
                else if (rnd < 2/3){    
                    this.simulatedVision += 1;
                    this.getInVisionSpots(this.simulatedVision, this.cardZone.location);
                }
                // Reveal
                else{
                    let rnd2 = Math.floor(Math.random()*this.mapSize*this.mapSize);
                    this.getInVisionSpots(1, GameController.Instance.shipSpotsArray[rnd2].location);
                }
                break;

            case GAMEPHASE.DEPLOY:
                var rndID = Math.floor( (Math.random()*this.cardLibrary.length-1) +1 );                
                this.tryCast(rndID);
                break;

            case GAMEPHASE.ENGAGE:
                var rndID = Math.floor( (Math.random()*this.cardLibrary.length-1) +1 );                
                this.tryCast(rndID);
                break;

            case GAMEPHASE.PROPEL:          // On turn end
                var rndID = Math.floor( (Math.random()*this.cardLibrary.length-1) +1 );
                this.tryCast(rndID);
                
                // Reset varible
                this.availableSpots = [];
                this.overlapSpots = [];
                this.simulatedVision = 1;
                break;

            default:
                console.log("AI do something 滑出去了");
                break;
        }
    }

    // Simulate fog system and SEARCH phase
    getInVisionSpots(size : number, location : cc.Vec2){
        // To get target ships
        GameController.Instance.shipsArray.forEach((cardZone : CardZone)=>{
            var isInVision : boolean = (this.cardZone.location.sub(location).mag() <= size);
            if (isInVision) {
                var isShipOverlap : boolean = false;
                GameController.Instance.shipSpotsArray.forEach((shipZone : CardZone)=>{
                    if (shipZone.location == cardZone.location){
                        isShipOverlap = true;
                        return ;
                    }
                    if (isShipOverlap) this.overlapSpots.push(cardZone);
                });
                    
                this.availableSpots.push(cardZone);
            }
        });
        // To get target ship spots
        GameController.Instance.shipSpotsArray.forEach((cardZone : CardZone)=>{
            var isInVision : boolean = (this.cardZone.location.sub(location).mag() <= size);
            if (isInVision) {
                this.availableSpots.push(cardZone);
            }
        });
    }


    tryCast(cardID : number) : boolean{
        var cardInfo = this.cardLibrary[cardID].data.getComponent(Card).cardInfo;
        var targets : CardZone[] = this.getValidSpots(cardInfo[this.phase]);
        
        if (targets.length <= 0) return false;

        var index = Math.floor( Math.random()*targets.length );
        var target = targets[index];
        // If this is ENGAGE, then buff attack by shiptype
        if (this.phase == GAMEPHASE.ENGAGE){
            let type = this.ship.type;
            if (type == SHIPTYPE.MOTHER){
                // buff interceptor
                if (cardInfo[GAMEPHASE.ENGAGE].function == CARDTYPE.INTERCEPTOR)
                    cardInfo[GAMEPHASE.ENGAGE].val++;
            }
            else if (type == SHIPTYPE.ATTACKER){
                // buff laser
                if (cardInfo[GAMEPHASE.ENGAGE].function == CARDTYPE.CANNON)
                    cardInfo[GAMEPHASE.ENGAGE].val++;
            }
            else if (type == SHIPTYPE.SCOUT){
                // buff cannon
                if (cardInfo[GAMEPHASE.ENGAGE].function == CARDTYPE.CANNON)
                    cardInfo[GAMEPHASE.ENGAGE].val++;
            }
            let a = {
                cardInfo : cardInfo, 
                cardZone : target
            };
            GameController.Instance.AITargetArray.push( a );
        }
        // If not propel phase, then behave normally
        else if (this.phase != GAMEPHASE.PROPEL){
            let a = {
                cardInfo : cardInfo, 
                cardZone : target
            };
            GameController.Instance.AITargetArray.push( a );
        }
        // If propel, then push it into pending array
        else {
            var key = target.location.x.toString() + ' ' + target.location.y.toString();
            
            if (!GameController.Instance.propelPendingArray[key])
                GameController.Instance.propelPendingArray[key] = [];
            GameController.Instance.propelPendingArray[key].push({
                node : this.node, 
                val : cardInfo[this.phase].val
            });
        }

        return true;
    }

    // Get all valid targets of this card
    getValidSpots(cardInfo : CardSubInfo) : CardZone[]{
        var result : CardZone[] = [];
        var min = cardInfo.minLimit, max = cardInfo.maxLimit;

        this.availableSpots.forEach((spot : CardZone)=>{
            if (spot.node == null) return;

            // STOP AI go to other ship position
            var distance = this.cardZone.location.sub(spot.location).mag();
            if (    // In range, no supporting rivals, no friendly fire, no move on other ship
                ( (distance >= min && distance <= max) || max >= 50) && (
                    ( this.phase == GAMEPHASE.DEPLOY && spot.getComponent(Ship) && spot.getComponent(Ship).team == this.ship.team) ||
                    ( this.phase == GAMEPHASE.ENGAGE && spot.getComponent(Ship) && (
                        (this.ship.team == 2 && spot.getComponent(Ship).team == 3) ||
                        (this.ship.team == 3 && spot.getComponent(Ship).team <= 2)
                    )) || 
                    (this.phase == GAMEPHASE.PROPEL)
                )
            ){
                var overlap : boolean = false;
                if (this.phase == GAMEPHASE.PROPEL){
                    this.overlapSpots.forEach((cardZone : CardZone)=>{
                        if (cardZone.location == spot.location){
                            overlap = true;
                            return;
                        }
                    })
                }
                if (!overlap)
                    result.push(spot);
            }   
        });

        return result;
    }
}
