
const {ccclass, property} = cc._decorator;

@ccclass
export default class AutoHideShipStatus extends cc.Component {

    // x = 0 ~ 200
    @property(cc.Node)
    thingToHide : cc.Node = null;
    @property(cc.Vec2)
    shift : cc.Vec2 = cc.v2(0, 0);
    @property(cc.Vec2)
    sensorShiftPos : cc.Vec2 = cc.v2(0, 0);

    @property(cc.Boolean)
    hideByDefault : boolean = true;
    isHidden : boolean = false;
    isHiddenForever : boolean = false;
    sensorPositionOn : cc.Vec2 = null;
    sensorPositionOff: cc.Vec2 = null;

    start(){
        this.sensorPositionOn = this.node.position;
        this.sensorPositionOff = this.sensorShiftPos;
        this.node.on(cc.Node.EventType.MOUSE_DOWN, this.mouseDown, this);

        if (this.hideByDefault)
            this.hide();
    }
    onDisable(){
        this.node.off(cc.Node.EventType.MOUSE_DOWN, this.mouseDown, this);
    }

    mouseDown(event : cc.Event.EventMouse){
        if (this.isHidden == false)
            this.hide();
        else 
            this.show();
    }

    manuallyDisplayCallback(){
        if (this.isHidden){
            this.show();

            this.scheduleOnce(()=>{
                if (this.isHidden == false){
                    this.hide();   
                }
            }, 0.8);
        }
    }

    hide(){
        if (this.isHidden == true) return;
        this.isHidden = true;
        this.node.position = this.sensorPositionOff;
        cc.tween(this.thingToHide)
            .to(0.25, {position : this.shift}, {easing : "cubicOut"})
        .start();
    }
    show(){
        if (this.isHidden == false && this.isHiddenForever) return;
        this.isHidden = false;
        this.node.position = this.sensorPositionOn;
        cc.tween(this.thingToHide)
            .to(0.25, {position :  cc.v2(0, 0)}, {easing : "cubicOut"})
        .start();
    }
    hideForever(){
        this.isHiddenForever = true;
        this.hide();
    }

}
