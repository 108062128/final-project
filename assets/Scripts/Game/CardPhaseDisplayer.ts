import GameController, { GAMEPHASE } from './GameController';

const {ccclass, property} = cc._decorator;

@ccclass
export default class CardPhaseDisplayer extends cc.Component {

    @property([cc.Node])
    phases : cc.Node[] = [];

    @property(cc.Color)
    phaseColor : cc.Color = cc.color(75, 230, 145);

    displayRotate(){
        this.phases.forEach((currentDisplayPhase: cc.Node, index: number) => {
            if(index == GameController.Instance.currentDisplayPhase) {
                currentDisplayPhase.scale = 1.15;
            }
            else {
                currentDisplayPhase.scale = 1;
            }
        })
    }
    

}
