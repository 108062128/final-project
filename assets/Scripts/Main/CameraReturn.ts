
const {ccclass, property} = cc._decorator;

@ccclass
export default class CameraReturn extends cc.Component {

    start () {
        this.node.position = cc.v2(0, 0);
    }
}
