import { GameManager } from '../Managers/GameManager';

const {ccclass, property} = cc._decorator;

@ccclass
export default class ChatInstance extends cc.Component {

    @property(cc.Label)
    otherName : cc.Label = null;
    @property(cc.Label)
    otherMessage : cc.Label = null;
    @property(cc.Label)
    myName : cc.Label = null;
    @property(cc.Label)
    myMessage : cc.Label = null;


    widget : cc.Widget = null;

    start () {
        this.widget = this.getComponent(cc.Widget);
        this.widget.target = this.node.parent;
        this.widget.isAlignLeft = true; 
        this.widget.isAlignRight = true; 
        this.widget.left = 0;
        this.widget.right = 0;
    }

    init(name : string, message : string){
        // My's message
        if (name == GameManager.Instance.username){
            this.otherName.node.active = false;
            this.otherMessage.node.active = false;
            this.myMessage.string = message;
            this.node.color = cc.color(115, 255, 143);
        }
        // Other's message
        else {
            this.myName.node.active = false;
            this.myMessage.node.active = false;
            this.otherName.string = name;
            this.otherMessage.string = message;
        }
    }

}
