import { GameManager } from '../Managers/GameManager';
import ChatInstance from './ChatInstance';

declare var firebase : any;
const {ccclass, property} = cc._decorator;

@ccclass
export default class ChatHandler extends cc.Component {

    @property(cc.Node)
    chatListContent :cc.Node = null;
    @property(cc.Prefab)
    chatInstance : cc.Prefab = null;
    
    @property(cc.EditBox)
    editbox : cc.EditBox = null;

    appendRoomInstance : any = null;

    start(){
        this.appendRoomInstance = (snapshot) => {
            var name = '';
            var message = '';

            // snapshot.key = randomPushID
            snapshot.forEach((childshot) => {
                // childshot.key = 'user' or 'message'
                if (childshot.key == 'user'){
                    name = childshot.val();
                }
                else if (childshot.key == 'message'){
                    message = childshot.val();
                }
            });

            var curNode = cc.instantiate(this.chatInstance);
            curNode.getComponent(ChatInstance).init(name, message);
            curNode.parent = this.chatListContent;
        };
    }

    // Enable/Disable will be called 
    // when calling MainManager's screen scroll method
    onEnable () {
        this.refreshChat();
        firebase.database().ref('chats/' + GameManager.Instance.roomID).on('child_added', (snapshot)=>{
            var name = '';
            var message = '';
    
            // snapshot.key = randomPushID
            snapshot.forEach((childshot) => {
                // childshot.key = 'user' or 'message'
                if (childshot.key == 'user'){
                    name = childshot.val();
                }
                else if (childshot.key == 'message'){
                    message = childshot.val();
                }
            });
    
            var curNode = cc.instantiate(this.chatInstance);
            curNode.getComponent(ChatInstance).init(name, message);
            curNode.parent = this.chatListContent;
        });
    }
    onDisable () {
        firebase.database().ref('chats/' + GameManager.Instance.roomID).off('child_added');
        this.refreshChat();
    }
    refreshChat(){
        this.chatListContent.removeAllChildren();
    }

    sendMessage(button : cc.Button){
        if (this.editbox.string != ''){
            var str = this.editbox.string;
            this.editbox.string = '';
            firebase.database().ref('chats/' + GameManager.Instance.roomID).push({
                message : str,
                user : GameManager.Instance.username
            });
        }
    }
}
