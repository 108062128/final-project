import RoomHandler from './RoomHandler';
import ChatHandler from './ChatHandler';
import { GameManager, SCENE } from '../Managers/GameManager';
import GameController from '../Game/GameController';

declare var firebase : any;
const {ccclass, property} = cc._decorator;

@ccclass
export class MainManager extends cc.Component {

    static Instance : MainManager = null;

    @property(RoomHandler)
    roomHandler : RoomHandler = null;
    @property(ChatHandler)
    chatHandler : ChatHandler = null;

    @property(cc.Node)
    createUsernameModal : cc.Node = null;
    @property(cc.EditBox)
    username : cc.EditBox = null;

    @property(cc.Node)
    singlePlayerModal : cc.Node = null;

    isNewUser : boolean = false;
    transition: cc.Node = null;
    placeHolderLabel: cc.Node = null;

    @property(cc.Label)
    sliderValue : cc.Label = null;
    _currentPlayerLimit : number = 2;


    onLoad () {
        // Special singleton, destroy old one instead of new one
        if (MainManager.Instance != null){
            MainManager.Instance.destroy();
        }
        MainManager.Instance = this;
        this.placeHolderLabel = cc.find('Canvas/Main Screen/UI/Create Room Modal/Room Name/PLACEHOLDER_LABEL');
        var action = cc.repeatForever(cc.sequence(cc.fadeOut(0.7),cc.fadeIn(0.7)));
        this.placeHolderLabel.runAction(action);
    }

    start(){
        // reset gameinfo
        GameManager.Instance.gameInfo = {
            mapSize : 5,
            playerCount : 4
        };
        GameManager.Instance.isTutorial = false;
    }

    // Screen view control and enable/disable chat/room
    mainToRoomScreen(){
        cc.tween(cc.find('Canvas/Main Camera'))
            .to(1, {position : cc.v2(960, 0)})
            .call(()=>{
                this.roomHandler.node.active = false;
                this.chatHandler.node.active = true;
            })
        .start();
    }
    roomToMainScreen(){
        cc.tween(cc.find('Canvas/Main Camera'))
            .to(1, {position : cc.v2(0, 0)})
            .call(()=>{
                this.roomHandler.node.active = true;
                this.chatHandler.node.active = false;
            })
        .start();
    }

    // Username Creation
    openCreateUsernameModal(){
        // TODO : Cool Transition
        this.createUsernameModal.active = true;
    }
    closeCreateUsernameModal(){
        // TODO : Cool Transition
        this.createUsernameModal.active = false;
    }
    createUsername(){
        if (this.username.string != ''){
            firebase.database().ref('users/' + GameManager.Instance.userUID).update({
                username : this.username.string
            })
            .then(()=>{
                this.closeCreateUsernameModal();
            });
        }
    }

    openSinglePlayerModal(){
        // TODO : Cool Transition
        this.singlePlayerModal.active = true;
    }
    closeSinglePlayerModal(){
        // TODO : Cool Transition
        this.singlePlayerModal.active = false;
    }
    singlePlayerLimitCallback(slider : cc.Slider){
        // NOTE : Change Slider Cap here
        // Valid : 2, 4, 6, 8 people
        this._currentPlayerLimit = (Math.round(slider.progress*3) + 1) * 2;
        slider.progress = (this._currentPlayerLimit-2) / 6;
        this.sliderValue.string = this._currentPlayerLimit.toString();
    }
    // start game
    startGame(event, customEventData){
        
        GameManager.Instance.isTutorial = true;
        if (customEventData == '100'){
            GameManager.Instance.gameInfo = {
                mapSize : parseInt(this.sliderValue.string) + 1,
                playerCount : parseInt(this.sliderValue.string)
            }
            GameManager.Instance.isTutorial = false;
        }

        this.scheduleOnce(function(){
            cc.director.loadScene(SCENE.SELECT);
        }, 1);

        this.transition = cc.find('Canvas/black transition');
        this.transition.active = true;
        this.transition.opacity = 0;

        cc.tween(this.transition)
            .to(1,{opacity: 255})
        .start();
    }
}
