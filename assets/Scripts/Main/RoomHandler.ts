import RoomInstance from "./RoomInstance";
import { GameManager } from '../Managers/GameManager';
import { MainManager } from './MainManager';

declare var firebase : any;
const {ccclass, property} = cc._decorator;

@ccclass
export default class RoomHandler extends cc.Component {

    @property(cc.Node)
    roomListContent :cc.Node = null;
    @property(cc.Prefab)
    roomInstance : cc.Prefab = null;

    @property(cc.Node)
    createRoomModal : cc.Node = null;
    @property(cc.EditBox)
    roomName : cc.EditBox = null;
    @property(cc.Label)
    sliderValue : cc.Label = null;

    _currentRoomLimit : number = 2;
    placeHolderLabel: cc.Node = null;
    // Enable/Disable will be called 
    // when calling MainManager's screen scroll method
    onEnable(){
        this.refreshRoom();
    }

    // refresh room list, also check for empty room
    // If a room is empty, then remove the room and chat
    refreshRoom(){
        this.roomListContent.removeAllChildren();
        firebase.database().ref('rooms/').once('value', (snapshot)=>{
            snapshot.forEach(childshot => {
                var roomName : string = childshot.key;
                var roomLimit : number = 4;
                var currentPeople : number = 0;

                // Get Room Info
                childshot.forEach(ccshot => {
                    if (ccshot.key == 'limit') 
                        roomLimit = ccshot.val();
                    else 
                        currentPeople++;
                });

                // Remove room and chat if no people in that room
                if (currentPeople <= 0){
                    firebase.database().ref('rooms/' + roomName).remove();
                    firebase.database().ref('chats/' + roomName).remove();
                }
                // Instantiate Room Instance
                else {
                    var curNode = cc.instantiate(this.roomInstance);
                    curNode.getComponent(RoomInstance).init(roomName, currentPeople, roomLimit, this.node);
                    curNode.parent = this.roomListContent;
                }
            });
        })
        // .then({

        // });
    }
    createRoom(){
        var name : string = '';
        if (this.roomName.string == '')
            name = this._makeid(8);
        else 
            name = this.roomName.string;

        // Create room data in DB
        firebase.database().ref('rooms/' + name).update({
            limit : this._currentRoomLimit,
            [GameManager.Instance.userUID] : true
        })
        
        // maybe update player stat to show status
        // maybe a friend system ???
        
        // Then change scene
        .then(()=>{
            GameManager.Instance.roomID = name;
            this.refreshRoom();

            // Small delay here, to show the room that was created
            this.scheduleOnce(()=>{
                // Move to Room Screen
                MainManager.Instance.mainToRoomScreen();
                // TODO : Start some user communcation system
                // TODO : in room , show their username
            }, 0.1);
        });
    }
    joinRoom(event : cc.Event){

        // Almost the same as Create room
        // event.target.name = room name = button Node's name
        
        firebase.database().ref('rooms/' + event.target.name).update({
            [GameManager.Instance.userUID] : true
        })
        
        // Then change scene
        .then(()=>{
            GameManager.Instance.roomID = event.target.name;
            this.refreshRoom();
            this.scheduleOnce(()=>{
                MainManager.Instance.mainToRoomScreen();
            }, 0.1);
        });
    }
    leaveRoom(){

        // Remove player data under the room data
        firebase.database().ref('rooms/' + GameManager.Instance.roomID).update({
            [GameManager.Instance.userUID] : null
        })

        // Then change scene
        .then(()=>{
            GameManager.Instance.roomID = '';

            // Tween first, then refresh
            MainManager.Instance.roomToMainScreen();
        });
    }   
    
    //#region Create room modal
    openCreateRoomModal(){
        // TODO : Cool Transition
        this.createRoomModal.active = true;
    }
    closeCreateRoomModal(){
        // TODO : Cool Transition
        this.createRoomModal.active = false;
    }
    roomLimitCallback(slider : cc.Slider){
        // NOTE : Change Slider Cap here
        // current : 2 people ~ 8 people
        this._currentRoomLimit = Math.round(slider.progress*6) + 2;
        slider.progress = (this._currentRoomLimit-2) / 6;
        this.sliderValue.string = this._currentRoomLimit.toString();
    }
    //#endregion

    //#region Utilities
    _makeid(length : number) {
        var result           = [];
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ000000123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
          result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
       }
       return result.join('');
    }
}

// window.onbeforeunload = confirmExit;
// function confirmExit() {
//     return "You have attempted to leave this page. Are you sure?";
// }