import RoomHandler from './RoomHandler';
import { MainManager } from './MainManager';

const {ccclass, property} = cc._decorator;

@ccclass
export default class RoomInstance extends cc.Component {

    @property(cc.Label)
    roomName : cc.Label = null;
    @property(cc.Label)
    current : cc.Label = null;
    @property(cc.Label)
    cap : cc.Label = null;

    // Show room info, and bind click event
    init(name : string, cur : number, cap : number, handler : cc.Node){
        // For Room Handler button CB to use
        this.node.name = name;

        // Set string of labels
        this.roomName.string = name;
        this.current.string = cur.toString();
        this.cap.string = cap.toString();

        var clickEventHandler = new cc.Component.EventHandler();
         clickEventHandler.target = handler; 
         clickEventHandler.component = "RoomHandler";
         clickEventHandler.handler = "joinRoom";

         var button = this.node.getComponent(cc.Button);
         button.clickEvents.push(clickEventHandler);
    }

}
