// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    popup: cc.Node = null;
    hover : boolean = false;
    roomButton: cc.Node = null;
    tutorialButton : cc.Node = null;
    @property({type:cc.AudioClip})
    hoverEffect: cc.AudioClip = null;
    @property({type: cc.AudioClip})
    pressEffect: cc.AudioClip = null;
    start(){
        this.popup = cc.find('Canvas/Main Screen/UI/POPUP');
        this.roomButton = cc.find('Canvas/Main Screen/UI/Rooms');
        this.tutorialButton = cc.find('Canvas/Main Screen/UI/Tutorial');
    }
    onLoad(){
            this.node.on('mouseenter', this.upscale, this);
            this.node.on('mouseleave', this.downscale, this)
    }
    click(){
        cc.audioEngine.setEffectsVolume(0.5);
        cc.audioEngine.playEffect(this.pressEffect, false);
        this.scheduleOnce(function(){
            cc.audioEngine.setEffectsVolume(1);
        },0.3)
    }
    upscale(){
        if(this.node.getComponent(cc.Button).interactable == true){
            cc.audioEngine.playEffect(this.hoverEffect, false);
            document.body.style.cursor = "pointer";
            cc.tween(this.node)
            .to(0.2, {scale: 1.1})
            .start()
        }
        
    }
    downscale(){
        if(this.node.getComponent(cc.Button).interactable == true){
            document.body.style.cursor = "auto";
            cc.tween(this.node)
            .to(0.2, {scale: 1.0})
            .start()
        }
    }
    Roompopup(){
        this.scheduleOnce(function(){
            this.roomButton.active = false;
            this.tutorialButton.active = false;
        }, 0.4);
        this.popup.active = true;
        this.popup.opacity = 0;
        this.popup.scale = 0.2;
        cc.tween(this.popup)
        .to(0.5,{scale: 1, opacity: 255},{easing:"quartInOut"})
        .start();
    }
    closepopup(){
        this.scheduleOnce(function(){
            this.roomButton.active = true;
            this.tutorialButton.active = true; 
        }, 0.2);
        
        cc.tween(this.popup)
        .to(0.5, {scale: 0.2, opacity: 0}, {easing: "quartInOut"})
        .call(()=>{this.popup.active = false})
        .start();
    }
    // update (dt) {}
}
