// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import { GameManager, SCENE } from '../Managers/GameManager';
const {ccclass, property} = cc._decorator;

@ccclass
export default class SelectControl extends cc.Component {
    
    timer: cc.Node = null;
    mother: cc.Node = null;
    scout: cc.Node = null;
    attacker: cc.Node = null;
    repick: cc.Node = null;
    ready: cc.Node = null;
    black: cc.Node = null;
    startcountdown : cc.Node = null;
    motherImage : cc.Node = null;
    scoutImage : cc.Node = null;
    attackerImage : cc.Node = null;
    pickship : cc.Node = null;
    fps: number = 500;
    counter: number = 0;
    time: number =60;
    startTime: number = 5;
    readyup: boolean = false;
    anim : cc.Animation = null;
    @property({
        type : cc.AudioClip
    })
    countdownEffect: cc.AudioClip = null;
    transition : cc.Node = null;
    private choice : number = -1;

    @property(cc.Label)
    hp : cc.Label = null;
    @property(cc.Label)
    additionInfo : cc.Label = null;

    start () {
        this.black = cc.find('Canvas/Main Camera/Button controller/Black');
        this.mother = cc.find('Canvas/Main Camera/Button controller/Button mother');
        this.scout = cc.find('Canvas/Main Camera/Button controller/Button scout');
        this.attacker = cc.find('Canvas/Main Camera/Button controller/Button attacker');
        this.ready = cc.find('Canvas/Main Camera/Button controller/ready');
        this.repick = cc.find('Canvas/Main Camera/Button controller/repick');
        this.timer = cc.find('Canvas/Main Camera/Timer');
        this.startcountdown = cc.find('Canvas/Main Camera/Button controller/start countdown');
        this.motherImage = cc.find('Canvas/Main Camera/Card display/Mother Image');
        this.scoutImage = cc.find('Canvas/Main Camera/Card display/Scout Image');
        this.attackerImage = cc.find('Canvas/Main Camera/Card display/Attacker Image');
        this.pickship = cc.find('Canvas/Main Camera/Card display/Name');
        this.anim = this.getComponent(cc.Animation);
        this.scheduleOnce(function(){
            this.fps = this.counter; 
        },1);
    }

    update (dt) {
        if(this.fps == this.counter){
            this.counter = 0;
            if(this.time>0){
                this.time--;
                if(this.time <= 10){
                    var color =  cc.color(255, 0, 0);
                    this.timer.color = color; 
                }
            }
            else{
                this.time = 0 ;
                this.randomShip();
            }         
            this.timer.getComponent(cc.Label).string =""+ this.time;
            if(this.readyup){
                this.startcountdown.getComponent(cc.Label).string =""+ this.startTime;
                if(this.startTime <= 0){
                    this.enterPlay();
                }
                else{
                    this.startTime--; 
                    cc.audioEngine.playEffect(this.countdownEffect, false);  
                }
            }
        }
        else{
            this.counter++; 
        }
    
    }
    randomShip(){
        this.choice = (Math.floor(Math.random()*10)+1)%3 + 1;
        this.scheduleOnce(function(){
            this.enterPlay();
        },1);
    }
    selectship(event, customEventData) {
        this.ready.getComponent(cc.Button).interactable = true;

        if(customEventData == 'Mother'){
            this.choice = SHIPTYPE.MOTHER;
            this.pickship.getComponent(cc.Label).string = "Mother";
            this.motherImage.active = true;  
            this.scoutImage.active = false;
            this.attackerImage.active = false;
            this.hp.string = "HP : 15";
            this.additionInfo.string = "Interceptor++";
        }
        else if(customEventData == 'attacker'){
            this.choice = SHIPTYPE.ATTACKER;
            this.pickship.getComponent(cc.Label).string = "Attacker";
            this.motherImage.active = false;  
            this.scoutImage.active = false;
            this.attackerImage.active = true;
            this.hp.string = "HP : 12";
            this.additionInfo.string = "Laser++";
        }
        else if(customEventData == 'scout'){
            this.choice = SHIPTYPE.SCOUT;
            this.pickship.getComponent(cc.Label).string = "Scout";
            this.motherImage.active = false;  
            this.scoutImage.active = true;
            this.attackerImage.active = false;
            this.hp.string = "HP : 10";
            this.additionInfo.string = "Cannon++";
        }
    }
    pickReady(){
        this.scheduleOnce(function(){
            this._set();
        },0.5);  
    }
    pickRepick(){
        this.scheduleOnce(function(){
            this.startTime = 5;
            this.startcountdown.getComponent(cc.Label).string ="5";
            this._set();
        },0);
    }
    _set(){
        this.readyup = !this.readyup;
        this.startcountdown.opacity = 255 -  this.startcountdown.opacity;
        this.timer.opacity = 255-this.timer.opacity;
        this.black.opacity = 200 - this.black.opacity;
        this.repick.getComponent(cc.Button).interactable = !this.repick.getComponent(cc.Button).interactable;
        this.ready.getComponent(cc.Button).interactable = !this.ready.getComponent(cc.Button).interactable;
        this.attacker.getComponent(cc.Button).interactable = !this.attacker.getComponent(cc.Button).interactable ;
        this.mother.getComponent(cc.Button).interactable = !this.mother.getComponent(cc.Button).interactable;
        this.scout.getComponent(cc.Button).interactable = !this.scout.getComponent(cc.Button).interactable;
    }
    enterPlay(){
        cc.director.preloadScene(SCENE.GAME);

        this.scheduleOnce(function(){
            cc.director.loadScene(SCENE.GAME);
        },1);

        //這裡要把飛船資訊丟給其他的人
        //  this.choice是飛船資訊 1是mother, 2是attacker, 3是scout
        //時間走完，玩家沒有選擇的話，會random 給他一艘

        if (GameManager.Instance)
            GameManager.Instance.shipChoice = this.choice;

        this.transition = cc.find('Canvas/black transition');
        this.transition.active = true;
        this.transition.opacity = 0;
        cc.tween(this.transition)
        .to(0.8,{opacity: 255})
        .start();
    }
}

export enum SHIPTYPE {
    NONE = 0,
    MOTHER,
    ATTACKER,
    SCOUT
}