
const {ccclass, property} = cc._decorator;

@ccclass
export default class ClickItUp extends cc.Component {

    start () {
        cc.tween(this.node)
            .by( 0.5, 
                { position : cc.v2(0, 10)},
                { easing : 'backIn'}
            )
            .delay(0.1)
            .by( 0.5, 
                { position : cc.v2(0, -10)},
                { easing : 'backOut'}
            )
            .delay(0.1)
            .union()
            .repeatForever()
        .start();
    }


}
