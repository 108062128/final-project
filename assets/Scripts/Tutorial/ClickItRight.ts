
const {ccclass, property} = cc._decorator;

@ccclass
export default class ClickItRight extends cc.Component {

    start () {
        cc.tween(this.node)
            .by( 0.5, 
                { position : cc.v2(10, 0)},
                { easing : 'backIn'}
            )
            .delay(0.1)
            .by( 0.5, 
                { position : cc.v2(-10, 0)},
                { easing : 'backOut'}
            )
            .delay(0.1)
            .union()
            .repeatForever()
        .start();
    }


}