
const {ccclass, property, disallowMultiple, executionOrder} = cc._decorator;

@ccclass
@disallowMultiple
@executionOrder(1000)
export class InputManager extends cc.Component {

    static Instance : InputManager = null;

    @property([cc.Boolean])
    keys = [];
    @property([cc.Boolean])
    keyDown = [];

    _lastKey : number = 0;
    _lastKeyIndex : number = 0;
    @property({type : [cc.Integer]})
    _lastKeyQueue : number[] = [];
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        if (InputManager.Instance != null){
            this.node.destroy();
            console.log("Duplicated InputManager found !!!");
        }
        else {
            InputManager.Instance = this;
            cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this._onKeyDown, this);
            cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this._onKeyUp, this);
            cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this._onKeyDownDown, this);
            cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this._onKeyUpUp, this);
            for(var i = 0; i<1000; i++){
                this.keys[i] = false;
                this.keyDown[i] = false;
                this._lastKeyQueue[i] = 0;                
            }
        }
    }
    start(){
        cc.game.addPersistRootNode(this.node);
    }
    onDestroy(){
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this._onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this._onKeyUp, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this._onKeyDownDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this._onKeyUpUp, this);
    }

    update (dt : number) {
        for (var i=0; i<this._lastKeyIndex; i++){
            this.keyDown[this._lastKeyQueue[i]] = false;
        }
        this._lastKeyIndex = 0;
    }

    //#region Private Methods
    _onKeyDown(event : cc.Event.EventKeyboard){
        this.keys[event.keyCode] = true;
    }
    _onKeyUp(event : cc.Event.EventKeyboard){
        this.keys[event.keyCode] = false;
    }
    _onKeyDownDown(event : cc.Event.EventKeyboard){
        if (this._lastKey == event.keyCode)
            return ;

        this._lastKey = event.keyCode;
        this._lastKeyQueue[this._lastKeyIndex++] = this._lastKey;
        this.keyDown[this._lastKey] = true;
    }
    _onKeyUpUp(event : cc.Event.EventKeyboard){
        this.keyDown[event.keyCode] = false;
    }
    //#endregion
}
