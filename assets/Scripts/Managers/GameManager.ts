import { InputManager } from "./InputManager";
import { SHIPTYPE } from '../Select/SelectControl';

const {ccclass, property, disallowMultiple} = cc._decorator;

@ccclass
@disallowMultiple
export class GameManager extends cc.Component {

    static Instance : GameManager = null;

    @property({type : cc.AudioClip})
    BGM : cc.AudioClip = null;

    userUID : string = '';
    username : string = '';
    userEmail : string = '';

    gameInfo : {mapSize : number, playerCount : number} = {mapSize : 5, playerCount : 4};
    isTutorial : boolean = false;

    roomID : string = '';
    shipChoice : SHIPTYPE = SHIPTYPE.NONE;

    onLoad () {
        if (GameManager.Instance != null){
            this.node.destroy();
            console.log("Duplicated GameManager found !!!");
        }
        else {
            GameManager.Instance = this;
        }
    }

    start () {
        cc.game.addPersistRootNode(this.node);
        cc.audioEngine.playMusic(this.BGM, true);
    }
}

export enum SCENE {
    INDEX = 'Index',
    MAIN = 'Main',
    GAME = 'Game',
    SELECT = 'Select'
}