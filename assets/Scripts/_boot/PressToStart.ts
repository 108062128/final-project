import { GameManager, SCENE } from "../Managers/GameManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PressToStart extends cc.Component {
    text: cc.Node = null;
    start () {
        this.node.on(cc.Node.EventType.MOUSE_DOWN, this._continue);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this._continue);
    }
    onLoad(){
        this.text = cc.find('Canvas/Press To Start');
        var action = cc.repeatForever(cc.sequence(cc.fadeOut(2),cc.fadeIn(2)));
        this.text.runAction(action);
    }

    onDestroy(){
        this.node.off(cc.Node.EventType.MOUSE_DOWN, this._continue);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this._continue);
    }
    _continue(){ 
        cc.director.loadScene(SCENE.INDEX);
    }
    
}

